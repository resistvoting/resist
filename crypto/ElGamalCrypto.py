
import nacl.utils
import nacl.secret
import nacl.encoding
import nacl.exceptions
import nacl.hash

import crypto.number as number
import math
import json
import uuid

from Crypto.PublicKey import ElGamal as PyCryptoElgamal
from Crypto.Random import get_random_bytes, random
from Crypto.Util import number as cryptonumber

"""
This file is an interpretation of the parameter algorithm
described in the Appendix B (B.1) of Civitas.
http://drops.dagstuhl.de/opus/volltexte/2008/1296/pdf/07311.ClarksonMichael.Paper.1296.pdf

"""

"""
This file includes code that has been taken from an implementation of Threshold Elgamal Crypto.
The original author of that implementation was Tom Peterson.
The code was made publicly available with an MIT license.
https://github.com/tompetersen/threshold-crypto/tree/master/threshold_crypto 
"""


class CryptoError(Exception):
    pass


class ElGamalKeyParameters:

    @staticmethod
    def from_json(self, json_str: str):
        return self.from_dict(json.loads(json_str))

    @staticmethod
    def from_dict(obj: dict):
        return ElGamalKeyParameters(obj['p'], obj['q'], obj['g'])

    def __init__(self, p: int, q: int, g: int):
        """
        Construct key parameters. Required:
        - p = 2q + 1
        - g generates Z_q*, meaning (g^q mod p = 1) and (g^2 mod p != 1)
          These conditions are sufficient because subgroups of Z_p* can only have orders 1, 2, q or 2q.

        :param p: prime
        :param q: prime
        :param g: generator for Z_q*
        """
        if (2 * q + 1) != p:
            raise CryptoError('no safe prime (p = 2q + 1) given')
        if pow(g, q, p) != 1 or pow(g, 2, p) == 1:
            raise CryptoError('no generator g for subgroup of order q given')

        self._p = p
        self._q = q
        self._g = g

    @property
    def p(self) -> int:
        return self._p

    @property
    def q(self) -> int:
        return self._q

    @property
    def g(self) -> int:
        return self._g

    def to_dict(self):
        return {
            'p': self._p,
            'q': self._q,
            'g': self._g
        }

    def to_json(self) -> str:
        return json.dumps(self.to_dict())

    def to_dict_str(self):
        return {
            'p': str(self._p),
            'q': str(self._q),
            'g': str(self._g),
        }

    def to_json_str(self) -> str:
        return json.dumps(self.to_dict_str())

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self._p == other.p and
                self._q == other.q and
                self._g == other.g
                )

    def __str__(self):
        return 'ElGamalParameters:\n\tp = %d,\n\tq = %d,\n\tg = %d' % (self._p, self._q, self._g)


class ElGamalKey:

    @staticmethod
    def from_json(json_str: str):
        return ElGamalKey.from_dict(json.loads(json_str))

    def from_dict(obj: dict):
        key_params = ElGamalKeyParameters.from_dict(obj)
        return ElGamalKey(obj['x'], obj['y'], key_params)

    def __init__(self, x: int, y: int, key_params: ElGamalKeyParameters):
        if key_params is None:
            raise CryptoError('key parameters must be given')

        self._x = x
        self._y = y
        self._key_params = key_params

    @property
    def x(self) -> int:
        return self._x

    @property
    def y(self) -> int:
        return self._y

    @property
    def key_parameters(self) -> ElGamalKeyParameters:
        return self._key_params

    def to_dict(self):
        return {
            'p': self.key_parameters.p,
            'q': self.key_parameters.q,
            'g': self.key_parameters.g,
            'x': self.x,
            'y': self.y,
        }

    def to_json(self):
        return json.dumps(self.to_dict())

    def to_dict_str(self):
        return {
            'p': str(self.key_parameters.p),
            'q': str(self.key_parameters.q),
            'g': str(self.key_parameters.g),
            'x': str(self.x),
            'y': str(self.y),
        }

    def to_json_str(self) -> str:
        return json.dumps(self.to_dict_str())

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self.key_parameters == other.key_parameters and
                self.x == other.x and
                self.y == other.y)

    def __str__(self):
        return 'ElGamalKey:\n\tx = %d\n\ty = %d' % (self._x, self._y)


class PublicKey:
    """
    The public key (y mod p) linked to the (implicit) secret key (a) of the scheme.
    """

    @staticmethod
    def from_json(json_str: str):
        obj = json.loads(json_str)
        return PublicKey.from_dict(obj)

    @staticmethod
    def from_dict(obj: dict):
        key_params = ElGamalKeyParameters.from_dict(obj)
        return PublicKey(obj['y'], key_params)

    def __init__(self, y: int, key_params: ElGamalKeyParameters):
        """
        Construct the public key.

        :param y: the public key value
        :param key_params: the key parameters used for constructing the key.
        """
        if key_params is None:
            raise CryptoError('key parameters must be given')

        self._y = y
        self._key_params = key_params

    @property
    def y(self) -> int:
        return self._y

    @property
    def key_parameters(self) -> ElGamalKeyParameters:
        return self._key_params

    def to_dict(self):
        return {
            'p': self._key_params.p,
            'q': self._key_params.q,
            'g': self._key_params.g,
            'y': self._y,
        }

    def to_json(self):
        return json.dumps(self.to_dict())
    
    def to_dict_str(self):
        return {
            'p': str(self._key_params.p),
            'q': str(self._key_params.q),
            'g': str(self._key_params.g),
            'y': str(self._y),
        }
    
    def to_json_str(self) -> str:
        return json.dumps(self.to_dict_str())

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self.key_parameters == other.key_parameters and
                self.g_a == other.g_a)

    def __str__(self):
        return 'PublicKey:\n\ty = ' + str(self.y)


class PrivateKey:
    """
    The private key (x mod p) linked to the (implicit) secret key (a) of the scheme.
    """

    @staticmethod
    def from_json(json_str: str):
        obj = json.loads(json_str)
        return PrivateKey.from_dict(obj)

    @staticmethod
    def from_dict(obj: dict):
        key_params = ElGamalKeyParameters.from_dict(obj)
        return PrivateKey(obj['x'], key_params)

    def __init__(self, x: int, key_params: ElGamalKeyParameters):
        """
        Construct the public key.

        :param x: the private key value
        :param key_params: the key parameters used for constructing the key.
        """
        if key_params is None:
            raise CryptoError('key parameters must be given')

        self._x = x
        self._key_params = key_params

    @property
    def x(self) -> int:
        return self._x

    @property
    def key_parameters(self) -> ElGamalKeyParameters:
        return self._key_params

    def to_dict(self):
        return {
            'p': self._key_params.p,
            'q': self._key_params.q,
            'g': self._key_params.g,
            'x': self._x,
        }

    def to_json(self):
        return json.dumps(self.to_dict())
    
    def to_dict_str(self):
        return {
            'p': str(self._key_params.p),
            'q': str(self._key_params.q),
            'g': str(self._key_params.g),
            'x': str(self._x),
        }

    def to_json_str(self) -> str:
        return json.dumps(self.to_dict_str())

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self.key_parameters == other.key_parameters and
                self.x == other.x)

    def __str__(self):
        return 'PublicKey:\n\ty = ' + str(self.y)

class KeyShare:
    """
    A share (x_i, y_i) of the private key for share owner i.
    y_i is the evaluated polynom value of x_i in shamirs secret sharing.
    """

    @staticmethod
    def from_json(json_str: str):
        obj = json.loads(json_str)
        return KeyShare.from_dict(obj)

    @staticmethod
    def from_dict(obj: dict):
        key_params = ElGamalKeyParameters.from_dict(obj)
        return KeyShare(obj['x'], obj['y'], key_params)

    def __init__(self, x: int, y: int, key_params: ElGamalKeyParameters):
        """
        Construct a share of the private key.

        :param x: the x value of the share
        :param y: the y value of the share
        :param key_params:
        """
        if key_params is None:
            raise CryptoError('key parameters must be given')

        self._x = x
        self._y = y
        self._key_params = key_params

    @property
    def x(self) -> int:
        return self._x

    @property
    def y(self) -> int:
        return self._y

    @property
    def key_parameters(self) -> ElGamalKeyParameters:
        return self._key_params

    def to_dict(self):
        return {
            'p': self.key_parameters.p,
            'q': self.key_parameters.q,
            'g': self.key_parameters.g,
            'x': self.x,
            'y': self.y,
        }

    def to_json(self):
        return json.dumps(self.to_dict())
    
    def to_dict_str(self):
        return {
            'p': str(self.key_parameters.p),
            'q': str(self.key_parameters.q),
            'g': str(self.key_parameters.g),
            'x': str(self.x),
            'y': str(self.y),
        }
    
    def to_json_str(self) -> str:
        return json.dumps(self.to_dict_str())

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self.key_parameters == other.key_parameters and
                self.x == other.x and
                self.y == other.y)

    def __str__(self):
        return 'KeyShare:\n\tx = %d\n\ty = %d' % (self._x, self._y)


class EncryptedMessage:
    """
    An encrypted message in the scheme. Because a hybrid approach is used it consists of three parts:
    - a = g^r mod p as in the ElGamal scheme
    - b = m * g^r mod p as in the ElGamal scheme with m being the value to be encrypted
    - enc the symmetrically encrypted message.
    The symmetric key is derived from the ElGamal encrypted value m.
    """

    @staticmethod
    def from_json(json_str: str):
        obj = json.loads(json_str)
        return EncryptedMessage.from_dict(obj)

    @staticmethod
    def from_dict(obj: dict):
        return EncryptedMessage(obj['a'], obj['b'])

    def __init__(self, a: int, b: int):
        """
        Construct a encrypted message.

        :param a: like in ElGamal scheme
        :param b: like in ElGamal scheme
        """
        self._a = a
        self._b = b

    @property
    def a(self) -> int:
        return self._a

    @property
    def b(self) -> int:
        return self._b

    def to_dict(self):
        return {
            'a': self.a,
            'b': self.b,
        }

    def to_json(self):
        return json.dumps(self.to_dict())
    
    def to_dict_str(self):
        return {
            'a': str(self.a),
            'b': str(self.b),
        }
    
    def to_json_str(self) -> str:
        return json.dumps(self.to_dict_str())

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self.a == other.a and
                self.b == other.b)

    def __str__(self):
        return 'EncryptedMessage:\n\ta = %d\n\tb = %d' % (self._a, self._b)


class NMEncryptedMessage:
    """
    A non-malleable encrypted message in the scheme. Because a hybrid approach is used it consists of four parts:
    - a = g^r mod p as in the ElGamal scheme
    - b = m * g^r mod p as in the ElGamal scheme with m being the value to be encrypted
    - c = hash(g^s mod p, a, b) mod q
    - d = (s + c*r) mod q
    The symmetric key is derived from the ElGamal encrypted value m.
    """
    @staticmethod
    def from_json(json_str: str):
        obj = json.loads(json_str)
        return NMEncryptedMessage.from_dict(obj)

    @staticmethod
    def from_dict(obj: dict):
        return NMEncryptedMessage(obj['a'], obj['b'], obj['c'],obj['d'])

    def __init__(self, a: int, b: int, c: int, d: int):
        """
        Construct a encrypted message.

        :param a: like in ElGamal scheme
        :param b: like in ElGamal scheme
        """
        self._a = a
        self._b = b
        self._c = c
        self._d = d

    @property
    def a(self) -> int:
        return self._a

    @property
    def b(self) -> int:
        return self._b
    
    @property
    def c(self) -> int:
        return self._c
    
    @property
    def d(self) -> int:
        return self._d

    def to_dict(self):
        return {
            'a': self.a,
            'b': self.b,
            'c': self.c,
            'd': self.d,
        }

    def to_json(self):
        return json.dumps(self.to_dict())
    
    def to_dict_str(self):
        return {
            'a': str(self.a),
            'b': str(self.b),
            'c': str(self.c),
            'd': str(self.d),
        }

    def to_json_str(self):
        return json.dumps(self.to_dict_str())

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self.a == other.a and
                self.b == other.b and
                self.c == other.c and
                self.d == other.d)

    def __str__(self):
        return 'EncryptedMessage:\n\ta = %d\n\tb = %d' % (self._a, self._b)


class EncryptedCredential:
    """
    A non-malleable encrypted message in the scheme. Because a hybrid approach is used it consists of four parts:
    - a = g^r mod p as in the ElGamal scheme
    - b = m * g^r mod p as in the ElGamal scheme with m being the value to be encrypted
    - c = hash(g^s mod p, a, b) mod q
    - d = (s + c*r) mod q
    The symmetric key is derived from the ElGamal encrypted value m.
    """
    @staticmethod
    def from_json(json_str: str):
        obj = json.loads(json_str)
        return EncryptedCredential.from_dict(obj)

    @staticmethod
    def from_dict(obj: dict):
        return EncryptedCredential(obj['a'], obj['b'], obj['c'], obj['d'])

    def __init__(self, a: int, b: int, c: int, d: int):
        """
        Construct a encrypted message.

        :param a: like in ElGamal scheme
        :param b: like in ElGamal scheme
        """
        self._a = a
        self._b = b
        self._c = c
        self._d = d

    @property
    def a(self) -> int:
        return self._a

    @property
    def b(self) -> int:
        return self._b

    @property
    def c(self) -> int:
        return self._c

    @property
    def d(self) -> int:
        return self._d

    def to_dict(self):
        return {
            'a': self.a,
            'b': self.b,
            'c': self.c,
            'd': self.d,
        }

    def to_json(self):
        return json.dumps(self.to_dict())
    
    def to_dict_str(self):
        return {
            'a': str(self.a),
            'b': str(self.b),
            'c': str(self.c),
            'd': str(self.d),
        }
    
    def to_json_str(self):
        return json.dumps(self.to_dict_str())

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self.a == other.a and
                self.b == other.b and
                self.c == other.c and
                self.d == other.d)

    def __str__(self):
        return 'EncryptedCredential:\n\ta = %d\n\tb = %d' % (self._a, self._b)

class ElGamalCrypto :
    @staticmethod
    def static_512_key_parameters() -> ElGamalKeyParameters:
        p = 7452962895294639129334402125897500494888232626693057568141676237916133687836239813279595639173262006234190877985012715032067188198462763852940914332974923
        q = 3726481447647319564667201062948750247444116313346528784070838118958066843918119906639797819586631003117095438992506357516033594099231381926470457166487461
        g = 1291791552707048245090176929539921926555612768576578304996066408519254635531597933040589792119803333400907701386210407460215915386675734438575583315866662

        return ElGamalKeyParameters(p=p, q=q, g=g)

    @staticmethod
    def static_1024_key_parameters() -> ElGamalKeyParameters:
        p = 91926125049667098586079247877954763240710944754791290600171879145842202844582766445861279297301599420441349450624904680670028101087907202676187927298703453013677139241144888658781102160172206621554562245692688934838467931639759244893636532792266365772257680539051728298486982205394534973908616156393244207739
        q = 45963062524833549293039623938977381620355472377395645300085939572921101422291383222930639648650799710220674725312452340335014050543953601338093963649351726506838569620572444329390551080086103310777281122846344467419233965819879622446818266396133182886128840269525864149243491102697267486954308078196622103869
        g = 70818045059412229096505272235743689389634686118918223401102407992390386472684584481028357968153236944027127686728774077632802000371835496959768179869988907055346762739460056524883039694157166386370062954387939133203886044147537199513403531355494899326175829610159895944695633468594258300316101218892509250468

        return ElGamalKeyParameters(p=p, q=q, g=g)

    @staticmethod
    def static_2048_key_parameters() -> ElGamalKeyParameters:
        p = 21236934862218511653623447364710811485097463859632312183494470321865437684383737237095118043782630953663551424644349676344051737646575908035229445418970350579998208464171222735264502195941087943912660267879360376600529021906143975135547131090543473264136217654462045160443124694552287906754023396731536363634457064852718052465363129643486360313019964341460374106944280350493228300541853254188973394324275526119409082151906335458925803081691961627262872909544911250939426559669641181741418792650389621395244191383680090126152455331128151622579879305128105513594663146479955290034772381144185556023460209436347971723223
        q = 10618467431109255826811723682355405742548731929816156091747235160932718842191868618547559021891315476831775712322174838172025868823287954017614722709485175289999104232085611367632251097970543971956330133939680188300264510953071987567773565545271736632068108827231022580221562347276143953377011698365768181817228532426359026232681564821743180156509982170730187053472140175246614150270926627094486697162137763059704541075953167729462901540845980813631436454772455625469713279834820590870709396325194810697622095691840045063076227665564075811289939652564052756797331573239977645017386190572092778011730104718173985861611
        g = 17906435243842862345453835552815243404911647137073134463025160688995967488398983883106245967559373149795748443813049871354204770601174285653863536485679014760802852938205445698233430999295273593565745966645968651501470796614660949221379136782521156980440939610488754701132825226698633081421678693978649121584859440571365037320084373304699863718759566935084604920447588793835852015570107152381880424069367528899745747855775959412017686801090474997082475612446031206581849434853823922623275250452328283036003031659299664036862958862229602762586321240949493048947451750791435815161649573142989093624189059486316195524206

        return ElGamalKeyParameters(p=p, q=q, g=g)
    
    @staticmethod
    def commit(message: str) :
        return int.from_bytes(nacl.hash.blake2b(bytes(message, 'utf-8'),
                                         encoder=nacl.encoding.Base64Encoder), 'big')

    @staticmethod
    def generate_key_parameters(key_size: int) -> ElGamalKeyParameters:
        """
        Generates new random key parameters of size key_size.
        In the algorithm below, we are assuming that l and k are equal to key_size

        1. Select a random k-bit prime q.
        2. Select a random l-bit number p. Round p - 1 down to a multiple of 2q by setting p equal to
            p - (p mod 2q) + 1. Unless p is prime and |p| = l, repeat. After 2^(log2(l) + 2) tries,
            start over by picking a new q.
        3. h <- [1..p-1]; g = h^((p-1)/q) mod p; repeat until g ≢ 1 (mod p) and g ≢ -1 (mod p)
        4. Output (p, q, g)

        """
        key = PyCryptoElgamal.generate(key_size, get_random_bytes)
        p = key.p
        q, r = divmod(int(p - 1), 2)
        q = q + int(bool(r))
        print ("p = " + str(p) + "\nq = " + str(q))
        h = random.randrange(1, int(p-1))
        power, remainder = divmod(int(p - 1), int(q))
        power = power + int(bool(remainder))
        g = pow(int(h), int(power), int(p))
        return ElGamalKeyParameters(p = int(p), q = int(q) , g = int(g))

    @staticmethod
    def generate_key(key_size: int, key_params: ElGamalKeyParameters = None) -> ElGamalKey:
        #Commented out because all trustees need to have the same key parameters for a given election
        #if not(key_params):
        #    key_params = ElGamalKeyParameters.generate(key_size)
        if key_params is None :
            key_params = ElGamalCrypto.generate_key_parameters(key_size)
        x = number.getRandomRange(2, key_params.q - 2)
        y = pow(key_params.g, x, key_params.p)
        return ElGamalKey(x, y, key_params)
    
    @staticmethod
    def generate_dist_key(key_size: int, number_of_shares: int = 1, key_params: ElGamalKeyParameters = None) -> (PublicKey, PrivateKey, [KeyShare], [int]):
        if key_params is None:
            key_params = ElGamalCrypto.generate_key_parameters(key_size)
        y = 1
        x = 0
        key_shares = [None] * number_of_shares
        commitments = [None] * number_of_shares
        for i in range(0, number_of_shares):
            x_i = number.getRandomRange(2, key_params.q - 2)
            y_i = pow(key_params.g, x_i, key_params.p)
            key_shares[i] = KeyShare(
                x = x_i,
                y = y_i,
                key_params = key_params)
            commitments[i] = commit(str(y_i))
            y = (y * key_shares[i].y) % key_params.p
            x = (x + key_shares[i].x) % key_params.p
        public_key = PublicKey(y=y, key_params=key_params)
        private_key = PrivateKey(x=x, key_params=key_params)
        return (public_key, private_key, key_shares, commitments)
    
    @staticmethod
    def encode(m: int, params: ElGamalKeyParameters) -> int:
        return pow(params.g, m, params.p)

    @staticmethod
    def shared_random_value(params: ElGamalKeyParameters = None) -> int:
        if params is None:
            params = ElGamalCrypto.generate_key_parameters(2048)
        return number.getRandomRange(2, params.q-2) % params.p
    
    @staticmethod
    def encrypt(message: int, public_key: PublicKey, r: int = 0, encode: bool = False) -> EncryptedMessage:
        params = public_key.key_parameters
        if encode is True:
            message = ElGamalCrypto.encode(message, ElGamalKeyParameters(
                params.p, params.q, params.g))
        if(r == 0):
            r = number.getRandomRange(1, params.q)
        a = pow(params.g, r, params.p)
        b = ((message % params.p) * pow(public_key.y, r, params.p)) % params.p
        return EncryptedMessage(a, b)
    
    @staticmethod
    def reencrypt(encrypted_message: EncryptedMessage, public_key: PublicKey) -> EncryptedMessage:
        params = public_key.key_parameters
        r = number.getRandomRange(1, params.q)
        a = ((encrypted_message.a % params.p) * \
            pow(params.g, r, params.p)) % params.p
        b = ((encrypted_message.b % params.p) * \
            pow(public_key.y, r, params.p)) % params.p
        return EncryptedMessage(a, b)
    
    @staticmethod
    def decrypt(cipher: EncryptedMessage, private_key: PrivateKey, encoded: bool = False) -> int:
        """
        Implemented Fermat's little theorem.
        TODO: Check if gcd(a^x,p) = 1
        """
        params = private_key.key_parameters
        b = cipher.b
        a = cipher.a
        aPowerX = pow(a, private_key.x, params.p)
        m = (b * pow(aPowerX, params.p - 2, params.p)) % params.p
        if encoded == True:
            m = bsgs(params.g, m, params.p)
        return m
    
    @staticmethod
    def dist_decrypt(ciphertext: EncryptedMessage, key_shares: KeyShare = [], params: ElGamalKeyParameters = None, encoded: bool = False) -> int:
        if params is None:
            params = key_shares[0].key_parameters
        decrypted_shares = []
        a = 1
        for i in len(key_shares):
            decrypted_shares[i] = pow(ciphertext.a, key_shares[i].x, params.p)
            a = (a * decrypted_shares[i]) % params.p
        m = (ciphertext.b * pow(a, params.p - 2, params.p)) % params.p
        if encoded == True:
            m = bsgs(params.g, m, params.p)
        return m
        

    #Although Non-Malleable Encryption and Decryption are just mentioned in the appendices, they've been added here for the sake of it
    @staticmethod
    def nmencrypt(message: int, public_key: int) -> NMEncryptedMessage:
        params = public_key.key_parameters
        r = number.getRandomRange(1, params.q)
        s = number.getRandomRange(1, params.q)
        encrypted_message = ElGamalCrypto.encrypt(message, public_key, r, encode = True)
        valueToHash = {
            'first': pow(params.g, s, params.p),
            'second': encrypted_message.a,
            'third': encrypted_message.b,
        }
        c = int.from_bytes(nacl.hash.blake2b(bytes(json.dumps(valueToHash), 'utf-8'),
                                             encoder=nacl.encoding.Base64Encoder), 'big') % params.q
        d = (s + (c * r)) % params.q
        return NMEncryptedMessage(encrypted_message.a, encrypted_message.b, c, d)
    
    @staticmethod
    def nmdecrypt(encrypted_message: NMEncryptedMessage, private_key: PrivateKey) -> int:
        params = private_key.key_parameters
        valueToHash = {
            'first': (pow(params.g, encrypted_message.d) * pow(pow(encrypted_message.a, encrypted_message.c), params.p - 2, params.p)) % params.p,
            'second': encrypted_message.a,
            'third': encrypted_message.b,
        }
        verification = int.from_bytes(nacl.hash.blake2b(bytes(json.dumps(valueToHash), 'utf-8'),
                                                        encoder=nacl.encoding.Base64Encoder), 'big') % params.q
        if (verification == encrypted_message.c):
            return decrypt(EncryptedMessage(encrypted_message.a, encrypted_message.b), private_key, encoded=True)

    @staticmethod
    def credential_encrypt(credential_share: int, public_key: PublicKey, rid: str, vid: str, r: int = 0) -> EncryptedCredential:
        params = public_key.key_parameters
        if r == 0:
            r = number.getRandomRange(1, params.q)
        t = number.getRandomRange(1, params.q)
        encrypted_message = ElGamalCrypto.encrypt(credential_share, public_key, r)
        valueToHash = {
            'first': pow(params.g, t, params.p),
            'second': encrypted_message.a,
            'third': encrypted_message.b,
            'fourth': rid,
            'fifth': vid,
        }
        c = int.from_bytes(nacl.hash.blake2b(bytes(json.dumps(valueToHash), 'utf-8'),
                                             encoder=nacl.encoding.Base64Encoder), 'big') % params.q
        d = (t + (c * r)) % params.q
        return EncryptedCredential(encrypted_message.a, encrypted_message.b, c, d)
    
    @staticmethod
    def credential_verify(credential_share: EncryptedCredential, params: ElGamalKeyParameters, rid: str, vid: str) -> bool:
        valueToHash = {
            'first': (pow(params.g, credential_share.d, params.p) * pow(pow(credential_share.a, credential_share.c, params.p), params.p-2, params.p)) % params.p,
            'second': credential_share.a,
            'third': credential_share.b,
            'fourth': rid,
            'fifth': vid,
        }
        v = int.from_bytes(nacl.hash.blake2b(bytes(json.dumps(valueToHash), 'utf-8'),
                                             encoder=nacl.encoding.Base64Encoder), 'big') % params.q
        if v == credential_share.c:
            return True
        else:
            return False

    @staticmethod
    def homomorphic_multiply(encrypted_message_one : EncryptedMessage, encrypted_message_two : EncryptedMessage, public_key : PublicKey) -> EncryptedMessage :
        multiplied_message = {
            'a' : (encrypted_message_one.a * encrypted_message_two.a) % public_key.key_parameters.p,
            'b' : (encrypted_message_two.b * encrypted_message_two.b) % public_key.key_parameters.p,
        }
        result = EncryptedMessage.from_json(multiplied_message)
        return result

"""
    The code for the Baby-Step-Giant-Step Algorithm has been taken from a github gist.
    Github Username: 0xTowel
    (https://gist.github.com/0xTowel/b4e7233fc86d8bb49698e4f1318a5a73)
"""


def bsgs(g, h, p):
    '''
    Solve for x in h = g^x mod p given a prime p.
    If p is not prime, you shouldn't use BSGS anyway.
    '''
    N = math.ceil(math.sqrt(p - 1))  # phi(p) is p-1 if p is prime

    # Store hashmap of g^{1...m} (mod p). Baby step.
    tbl = {pow(g, i, p): i for i in range(N)}

    # Precompute via Fermat's Little Theorem
    c = pow(g, N * (p - 2), p)

    # Search for an equivalence in the table. Giant step.
    for j in range(N):
        y = (h * pow(c, j, p)) % p
        if y in tbl:
            return j * N + tbl[y]

    # Solution not found
    return None


def commit(message: str):
        return int.from_bytes(nacl.hash.blake2b(bytes(message, 'utf-8'),
                                                encoder=nacl.encoding.Base64Encoder), 'big')

def generate_fake_credentials(credential_shares : [int] = []):
    fake_credentials = []
    for i in range(0, len(credential_shares)):
        temp = []
        for j in range(0, len(credential_shares)):
            if (i != j):
                temp.append(random.getrandbits(512))
            else:
                temp.append(credential_shares[j])
        fake_credentials.append(temp)
    return fake_credentials