from ElGamalCrypto import (
    ElGamalCrypto,
    ElGamalKey,
    ElGamalKeyParameters,
    EncryptedMessage
)

keyParameters = ElGamalCrypto.static_512_key_parameters()
key = ElGamalCrypto.generateKey(512, keyParameters)
messageToEncrypt = 4
encryptedMessage = ElGamalCrypto.encrypt(messageToEncrypt, key, keyParameters)
print("Public Key: " + str(key.y) + "\nPrivate Key: " + str(key.x) + "\n")
print("Plaintext Message: " + str(messageToEncrypt) + "\nEncrypted Message:\n-a: " + str(encryptedMessage.a) + "\n-b: " + str(encryptedMessage.b) + "\n")
decryptedMessage = ElGamalCrypto.decrypt(encryptedMessage, key, keyParameters)
print("Decrypted Message: " + str(decryptedMessage))