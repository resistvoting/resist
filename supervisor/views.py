from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from bulletin.models import Election, CastVote
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from bulletin.models import Election, TabulationTeller, RegistrationTeller
from tabulation.models import TabulationTellerPrivate
from register.models import RegistrationTellerPrivate
from crypto import ElGamalCrypto as crypto

from threshold_crypto import ThresholdCrypto, ThresholdParameters

def is_member(user):
    return user.groups.filter(name='Supervisor').exists()

# Create your views here.
@login_required
@user_passes_test(is_member)
def index(request):
    return render(request, 'supervisor/index.html')

@login_required
@user_passes_test(is_member)
def createelection(request):
    registration_tellers = User.objects.filter(groups__name='Registration Teller')
    tabulation_tellers = User.objects.filter(groups__name='Tabulation Teller')
    return render(request, 'supervisor/createelection.html', {
        'registrationTellers' : registration_tellers,
        'tabulationTellers' : tabulation_tellers,
    })


@login_required
@user_passes_test(is_member)
def electionlist(request):
    user = request.user
    elections = Election.objects.filter(supervisor=user)
    return render(request, 'supervisor/electionlist.html', {
        'elections' : elections,
    })

@login_required
@user_passes_test(is_member)
def electionactions(request):
    return render(request, 'supervisor/electionactions.html', {
        'electionID' : request.POST['election_id']
    })
    

@login_required
@user_passes_test(is_member)
def generate_public_key(request):
    election = Election.objects.get(election_uuid=request.POST['electionID'])
    if election.public_key:
        return render(request, 'supervisor/electionpublickey.html', {
            'public_key' : election.public_key
        })
    else:
        try:
            tabulation_tellers = TabulationTeller.objects.filter(election=election)
        except:
            raise ValueError("No Tabulation Tellers found for election with id : " + str(request.POST['electionID']))

        (public_key, private_key, key_shares, commitments) = crypto.ElGamalCrypto.generate_dist_key(512, len(tabulation_tellers))
        shared_random_value = crypto.ElGamalCrypto.shared_random_value(public_key.key_parameters)
        for tabulation_teller, key_share in zip(tabulation_tellers, key_shares):
            tabulation_teller_private = TabulationTellerPrivate(user = tabulation_teller.user, election = election, key_share = key_share.to_json(), z = { 'value' : str(shared_random_value) })
            tabulation_teller_private.save()
            tabulation_teller.public_key_share = crypto.PublicKey(key_share.y, key_share.key_parameters).to_json()
            tabulation_teller.save()
        election.public_key = public_key.to_json()
        election.private_key = private_key.to_json()
        commitments_array = [None] * len(commitments)
        for i in range(0, len(commitments)):
            commitments_array[i] = {
                'value' : commitments[i]
            }
        election.key_commitments = commitments_array
        encrypt_one = crypto.ElGamalCrypto.encrypt(message = 1, public_key = public_key, encode = True)        
        encrypt_zero = crypto.ElGamalCrypto.encrypt(message = 0, public_key = public_key, encode = True)        
        election.encrypted_choices = [encrypt_zero.to_json(), encrypt_one.to_json()]
        election.save()
        return render(request, 'supervisor/electionpublickey.html', {
            'public_key' : public_key,
            'key_shares' : key_shares,
        })

@login_required
@user_passes_test(is_member)
def registration_keys(request):
    election = Election.objects.get(election_uuid=request.POST['electionID'])
    registration_tellers = RegistrationTeller.objects.filter(election = election)
    for registration_teller in registration_tellers:
        elgamal_key = crypto.ElGamalCrypto.generate_key(512)
        public_key = crypto.PublicKey(y = elgamal_key.y, key_params = elgamal_key.key_parameters)
        private_key = crypto.PrivateKey(x = elgamal_key.x, key_params = elgamal_key.key_parameters)
        registration_teller_private = RegistrationTellerPrivate(election = election, user = registration_teller.user, private_key = private_key.to_json())
        registration_teller.public_key = public_key.to_json()
        registration_teller_private.save()
        registration_teller.save()
    return render(request, 'supervisor/registrationkeys.html', {
        'keys_generated' : True,
    })

@login_required
@user_passes_test(is_member)
def freezeregistration(request):
    election = Election.objects.get(election_uuid=request.POST['electionID'])
    if (election.registration_frozen == False):
        election.registration_frozen = True
        election.save()
        return render(request, 'supervisor/freezereg.html', {
            'message' : 'Registration for election' + str(election) + 'has been frozen'
        })
    else:
        election.registration_frozen = False
        election.save()
        return render(request, 'supervisor/freezereg.html', {
            'message' : 'Registration for election' + str(election) + 'has been unfrozen'
        })


@login_required
@user_passes_test(is_member)
def freezevoting(request):
    pass


@login_required
@user_passes_test(is_member)
def releaseresults(request):
    pass

def addelection(request):
    election = Election()
    election.name = request.POST['election_name']
    election.organization = request.POST['election_organization']
    election.description = request.POST['election_description']
    election.help_email = request.POST['email']
    #TODO: Change the following to call key generators
    election.public_key = "Filler Public Key"
    election.private_key = "Filler Private Key"
    election.save()
    #TODO: change the redirect URL
    return HttpResponseRedirect(reverse('supervisor:createelection'))

@login_required
@user_passes_test(is_member)
def cheattally(request):
    election = Election.objects.get(supervisor = request.user)
    cast_votes = CastVote.objects.filter(election = election)
    private_key = crypto.PrivateKey.from_json(election.private_key)
    total = []
    
def createvoter(request):
    pass

def createtabulation(request):
    pass

def createauth(request):
    pass

def regvoter(request):
    pass
