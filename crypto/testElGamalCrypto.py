import unittest

from crypto.ElGamalCrypto import (
    ElGamalCrypto,
    ElGamalKey,
    ElGamalKeyParameters
)
import crypto.number

class ELGamalCryptoTest(unittest.TestCase):
    def setUp(self):
        self.EGParameters = ElGamalCrypto.static_512_key_parameters()
        self.EGKey = ElGamalCrypto.generateKey(2048, self.EGParameters)

    def testMessageEncoding(self):
        messageToEncode = 2
        testEncodedMessage = ElGamalCrypto.encode(m = messageToEncode, params = self.EGParameters)
        self.assertEqual(pow(self.EGParameters.g, 2, self.EGParameters.p), testEncodedMessage)
    
    def testMessageEncryptionDecryption(self):
        messageToEncrypt = 4
        self.assertEqual(4, ElGamalCrypto.decrypt(ElGamalCrypto.encrypt(messageToEncrypt, self.EGKey, self.EGParameters), self.EGKey, self.EGParameters))

if __name__ == "__main__":
    unittest.main()