# Resist
## Combatting Coercion in Verifiable Web-based Election
### Master's Thesis, Dalhousie University

Resist is an attempt at presenting coercion-resistant elections as being practical. It derives from a bunch of work done by amazing researchers in the past. 

### Warning: 
It is a simple demo to present what can be done. Do not deploy this code in any election whatsoever.

### Installation

1. Clone this repository.

2. Navigate to the top-level directory.

```
cd resist
```

3. Create a virtual environment.

```
virtualenv venv
```

4. Activate virtual environment.

```
source venv/bin/activate
```

5. Install requirements.

```
python3 -m pip install -r requirements.txt
```

6. Alter database configurations in `resist/settings.py`.

7. Migrate models to the database.

```
python3 manage.py makemigrations
python3 manage.py migrate
```

8. Start server.

```
python3 manage.py runserver
```

9. Open your browser and navigate to `localhost:8000`.