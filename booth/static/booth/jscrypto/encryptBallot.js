
function encryptBallot(ballot) {
    electionPublicKey = ballot.electionMetadata.electionPublicKey;
    elGamalParameters = {
      p: bigInt(electionPublicKey.p),
      q: bigInt(electionPublicKey.q),
      g: bigInt(electionPublicKey.g),
    };
    ballot_encrypted_choices = ballot.electionMetadata.encrypted_choices
    console.log("Params:");
    console.log(elGamalParameters);
    console.log("Public Key:");
    console.log(bigInt(electionPublicKey.p));
    encrypted_choices = []
    ballot.questions.forEach( function(question, questionindex) {
        question.choices.forEach( function (choice, choiceindex) {
            if (choice.selection === 0) {
                encrypted_choices.push(elGamalReEncrypt(ballot_encrypted_choices[0], bigInt(electionPublicKey.y), elGamalParameters));
            } else if (choice.selection === 1) {
                encrypted_choices.push(elGamalReEncrypt(ballot_encrypted_choices[1], bigInt(electionPublicKey.y), elGamalParameters));
            }
        });
    });
    console.log("Encrypted Choices : ");
    console.log(encrypted_choices);
    return encrypted_choices;
}