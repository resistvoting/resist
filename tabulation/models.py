from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.contrib.postgres.fields import JSONField
from django.conf import settings
from bulletin.models import Election

# Create your models here.
class TabulationTellerPrivate(models.Model):

    #Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    key_share = JSONField(default=dict, null=True)
    z = JSONField(default=dict, null=True)

    # Relationship Fields
    election = models.ForeignKey(
        Election,
        on_delete=models.CASCADE, related_name="tabulationtellerprivate"
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE, related_name="tabulationtellerprivate"
    )
