# Generated by Django 2.2.2 on 2019-06-20 12:24

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('bulletin', '0002_auto_20190620_0858'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='election',
            name='election_uuid',
        ),
        migrations.AddField(
            model_name='election',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4, editable=False, unique=True),
        ),
    ]
