function displayMetadata(ballot){
    console.log(ballot);
    document.getElementById('description').innerHTML = "";
    //Add Election Organization and Election Name as Titles
    let title = document.createElement("H1");
    let electionOrganization = document.createElement("strong");
    electionOrganization.innerHTML = ballot.electionMetadata.electionOrganization;
    title.appendChild(electionOrganization);
    let title1 = document.createElement("H2");
    let electionName = document.createTextNode(ballot.electionMetadata.electionName+"\n");
    title1.appendChild(electionName);
    //Add Election Description just below the Title
    let desc = document.createElement("p");
    let electionUUID = document.createTextNode("UUID:" + ballot.electionMetadata.electionUUID + "\n");
    let electionDesc = document.createTextNode(ballot.electionMetadata.electionDescription + "\n");
    let electionHelpEmail = document.createTextNode("Help Email:" + ballot.electionMetadata.electionHelpEmail + "\n");
    desc.appendChild(electionUUID);
    desc.appendChild(document.createElement("BR"));
    desc.appendChild(electionDesc);
    desc.appendChild(document.createElement("BR"));
    desc.appendChild(electionHelpEmail);
    let divElement = document.createElement("div");
    divElement.id = "electionMetadata";
    divElement.appendChild(title);
    divElement.appendChild(title1);
    divElement.appendChild(desc);
    let elementToAppend = document.getElementById("description");
    if(elementToAppend != null){
        elementToAppend.appendChild(divElement);
    }
    document
        .getElementById("button-begin")
        .style
        .display = "block";
    document
      .getElementById("button-begin")
      .addEventListener("click", function() {
          document
          .getElementById("button-begin")
          .style
          .display = null;
          displayBallot(ballot = ballot, currentQuestionNumber = 0)
      });
}