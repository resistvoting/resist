from django.shortcuts import render, get_object_or_404
from bulletin.models import Election, TabulationTeller

# Create your views here.
def tabulation(request, electionID, tabulationID) :
    try :
        election = Election.objects.get(uuid = electionID)
        try:
            tabulation_teller = election.tabulationteller_set.get(pk=tabulationID)
        except (KeyError, TabulationTeller.DoesNotExist):
            return render(request, 'tabulation/error.html', {
                'electionID': electionID,
                'tabulationID': tabulationID,
                'error_message': "A tabulation teller with the given id does not exist for this election"
            })
    except (Election.DoesNotExist) :
        return render(request, 'tabulation/error.html', {
            'electionID': electionID,
            'tabulationID': tabulationID,
            'error_message': "An election with the given id does not exist for this election"
        })
    return render(request, 'tabulation/index.html', {
        'electionID' : electionID,
        'tabulationID' : tabulationID,
    })

def tallyvotes(request) :
    election_id =  request.POST['electionID']
    tabulation_id = request.POST['tabulationID']
    private_key_share = int(request.POST['privateKey'])
    
    return render(request, 'tabulation/error.html', {
        'electionID': request.POST['electionID'],
        'tabulationID': request.POST['tabulationID'],
        'error_message': "Given Private Key Share was \n" + request.POST['privateKey'],
    })
