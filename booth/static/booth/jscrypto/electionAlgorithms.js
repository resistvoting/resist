function elGamalEncrypt(message, publicKey, params) {
  r = bigInt.randBetween("1", params.p);
  return {
    a: bigInt(params.g).modPow(r, params.p),
    b: bigInt(publicKey)
      .modPow(r, params.p)
      .multiply(bigInt(params.g).modPow(message, params.p))
      .mod(params.p)
  };
}
function elGamalReEncrypt(ciphertext, publicKey, params) {
  r = bigInt.randBetween("1", params.p);
  return {
    a: bigInt(ciphertext.a)
      .multiply(bigInt(params.g).modPow(r, params.p))
      .mod(params.p),
    b: bigInt(ciphertext.b)
      .multiply(bigInt(publicKey).modPow(r, params.p))
      .mod(params.p)
  };

}