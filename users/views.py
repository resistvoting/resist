from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.conf import settings
from django.contrib.auth.models import User

# Create your views here.
def index(request) :
    if request.user.is_authenticated:
        if request.user.groups.filter(name='Supervisor').exists():
            return redirect('supervisor/index')
        if request.user.groups.filter(name='Voter').exists():
            return redirect('booth/registerindex')
    return render(request, 'resist/index.html', { 'loginUrl' : settings.LOGIN_URL})

def registeruser(request) :
    if request.method == 'POST' :
        form = UserCreationForm(request.POST)
        if form.is_valid() :
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username = username, password = password)
            login(request, user)
            return redirect('users:index')
    else :
        form = UserCreationForm()
    context = { 'form' : form }
    return render(request, 'registration/register.html', context)
