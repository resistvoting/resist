function elGamalEncrypt(message, publicKey, params) {
    r = bigInt.randBetween("1", params['p']);
    return {
      a: bigInt(params.g).modPow(r, params.p),
      b: bigInt(publicKey)
        .modPow(r, params.p)
        .multiply(bigInt(params.g).modPow(message, params.p))
        .remainder(params.p)
    };
}