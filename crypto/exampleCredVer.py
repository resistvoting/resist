from credentialAlgs import (
    EncryptedCredential,
    CredentialCrypto
)
from ElGamalCrypto import (
    ElGamalCrypto,
    ElGamalKey,
    ElGamalKeyParameters,
    EncryptedMessage,
    NMEncryptedMessage
)
import number

keyParameters = ElGamalCrypto.static_512_key_parameters()
key = ElGamalCrypto.generateKey(512, keyParameters)
credentialShare = 4
randomizationFactor = number.getRandomRange(1, keyParameters.q)
rid = 1
vid = 2
encryptedCredentialShare = CredentialCrypto.encrypt(credentialShare, key.y, keyParameters, randomizationFactor, rid, vid)
print("Plaintext Credential Share: " + str(credentialShare))
print(encryptedCredentialShare)
CredentialCrypto.credVer(encryptedCredentialShare, keyParameters, rid, vid)