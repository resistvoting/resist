import unittest
from django.urls import reverse
from django.test import Client
from .models import Election, EligibleVoter, AuthenticatedVoter, RegisteredVoter, Credential, CredentialShare, RegistrationTeller, CastVote, TabulationTeller, FinalTally, Question, Choice
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_election(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["uuid"] = "uuid"
    defaults["organization"] = "organization"
    defaults["description"] = "description"
    defaults["public_key"] = "public_key"
    defaults["registration_frozen"] = "registration_frozen"
    defaults["voting_frozen"] = "voting_frozen"
    defaults["results_released"] = "results_released"
    defaults["help_email"] = "help_email"
    defaults.update(**kwargs)
    if "supervisor" not in defaults:
        defaults["supervisor"] = create_django_contrib_auth_models_user()
    return Election.objects.create(**defaults)


def create_eligiblevoter(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults.update(**kwargs)
    if "election" not in defaults:
        defaults["election"] = create_election()
    if "user" not in defaults:
        defaults["user"] = create_django_contrib_auth_models_user()
    return EligibleVoter.objects.create(**defaults)


def create_authenticatedvoter(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["authentication_key_public"] = "authentication_key_public"
    defaults["designation_key_public"] = "designation_key_public"
    defaults.update(**kwargs)
    if "eligible_voter" not in defaults:
        defaults["eligible_voter"] = create_eligiblevoter()
    return AuthenticatedVoter.objects.create(**defaults)


def create_registeredvoter(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["public_index"] = "public_index"
    defaults.update(**kwargs)
    if "authenticated_voter" not in defaults:
        defaults["authenticated_voter"] = create_authenticatedvoter()
    return RegisteredVoter.objects.create(**defaults)


def create_credential(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["encrypted_credential"] = "encrypted_credential"
    defaults.update(**kwargs)
    if "registered_voter" not in defaults:
        defaults["registered_voter"] = create_registeredvoter()
    return Credential.objects.create(**defaults)


def create_credentialshare(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["encrypted_credential_share"] = "encrypted_credential_share"
    defaults.update(**kwargs)
    if "registration_teller" not in defaults:
        defaults["registration_teller"] = create_registrationteller()
    if "credential" not in defaults:
        defaults["credential"] = create_credential()
    return CredentialShare.objects.create(**defaults)


def create_registrationteller(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults.update(**kwargs)
    if "election" not in defaults:
        defaults["election"] = create_election()
    if "user" not in defaults:
        defaults["user"] = create_django_contrib_auth_models_user()
    return RegistrationTeller.objects.create(**defaults)


def create_castvote(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["encrypted_credential"] = "encrypted_credential"
    defaults["encrypted_choices"] = "encrypted_choices"
    defaults["encrypted_index"] = "encrypted_index"
    defaults["proof_credential"] = "proof_credential"
    defaults["proof_choices"] = "proof_choices"
    defaults["proof_index"] = "proof_index"
    defaults.update(**kwargs)
    if "election" not in defaults:
        defaults["election"] = create_election()
    return CastVote.objects.create(**defaults)


def create_tabulationteller(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["public_key_share"] = "public_key_share"
    defaults.update(**kwargs)
    if "election" not in defaults:
        defaults["election"] = create_election()
    return TabulationTeller.objects.create(**defaults)


def create_finaltally(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["choice_tally"] = "choice_tally"
    defaults["proof_tally"] = "proof_tally"
    defaults.update(**kwargs)
    if "election" not in defaults:
        defaults["election"] = create_election()
    return FinalTally.objects.create(**defaults)


def create_question(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["text"] = "text"
    defaults["description"] = "description"
    defaults["max_choices"] = "max_choices"
    defaults.update(**kwargs)
    if "election" not in defaults:
        defaults["election"] = create_election()
    return Question.objects.create(**defaults)


def create_choice(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["text"] = "text"
    defaults["votes"] = "votes"
    defaults.update(**kwargs)
    if "question" not in defaults:
        defaults["question"] = create_question()
    return Choice.objects.create(**defaults)


class ElectionViewTest(unittest.TestCase):
    '''
    Tests for Election
    '''

    def setUp(self):
        self.client = Client()

    def test_list_election(self):
        url = reverse('bulletin_election_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_election(self):
        url = reverse('bulletin_election_create')
        data = {
            "name": "name",
            "uuid": "uuid",
            "organization": "organization",
            "description": "description",
            "public_key": "public_key",
            "registration_frozen": "registration_frozen",
            "voting_frozen": "voting_frozen",
            "results_released": "results_released",
            "help_email": "help_email",
            "supervisor": create_django_contrib_auth_models_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_election(self):
        election = create_election()
        url = reverse('bulletin_election_detail', args=[election.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_election(self):
        election = create_election()
        data = {
            "name": "name",
            "uuid": "uuid",
            "organization": "organization",
            "description": "description",
            "public_key": "public_key",
            "registration_frozen": "registration_frozen",
            "voting_frozen": "voting_frozen",
            "results_released": "results_released",
            "help_email": "help_email",
            "supervisor": create_django_contrib_auth_models_user().pk,
        }
        url = reverse('bulletin_election_update', args=[election.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class EligibleVoterViewTest(unittest.TestCase):
    '''
    Tests for EligibleVoter
    '''

    def setUp(self):
        self.client = Client()

    def test_list_eligiblevoter(self):
        url = reverse('bulletin_eligiblevoter_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_eligiblevoter(self):
        url = reverse('bulletin_eligiblevoter_create')
        data = {
            "name": "name",
            "election": create_election().pk,
            "user": create_django_contrib_auth_models_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_eligiblevoter(self):
        eligiblevoter = create_eligiblevoter()
        url = reverse('bulletin_eligiblevoter_detail',
                      args=[eligiblevoter.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_eligiblevoter(self):
        eligiblevoter = create_eligiblevoter()
        data = {
            "name": "name",
            "election": create_election().pk,
            "user": create_django_contrib_auth_models_user().pk,
        }
        url = reverse('bulletin_eligiblevoter_update',
                      args=[eligiblevoter.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class AuthenticatedVoterViewTest(unittest.TestCase):
    '''
    Tests for AuthenticatedVoter
    '''

    def setUp(self):
        self.client = Client()

    def test_list_authenticatedvoter(self):
        url = reverse('bulletin_authenticatedvoter_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_authenticatedvoter(self):
        url = reverse('bulletin_authenticatedvoter_create')
        data = {
            "name": "name",
            "authentication_key_public": "authentication_key_public",
            "designation_key_public": "designation_key_public",
            "eligible_voter": create_eligiblevoter().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_authenticatedvoter(self):
        authenticatedvoter = create_authenticatedvoter()
        url = reverse('bulletin_authenticatedvoter_detail',
                      args=[authenticatedvoter.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_authenticatedvoter(self):
        authenticatedvoter = create_authenticatedvoter()
        data = {
            "name": "name",
            "authentication_key_public": "authentication_key_public",
            "designation_key_public": "designation_key_public",
            "eligible_voter": create_eligiblevoter().pk,
        }
        url = reverse('bulletin_authenticatedvoter_update',
                      args=[authenticatedvoter.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RegisteredVoterViewTest(unittest.TestCase):
    '''
    Tests for RegisteredVoter
    '''

    def setUp(self):
        self.client = Client()

    def test_list_registeredvoter(self):
        url = reverse('bulletin_registeredvoter_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_registeredvoter(self):
        url = reverse('bulletin_registeredvoter_create')
        data = {
            "name": "name",
            "public_index": "public_index",
            "authenticated_voter": create_authenticatedvoter().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_registeredvoter(self):
        registeredvoter = create_registeredvoter()
        url = reverse('bulletin_registeredvoter_detail',
                      args=[registeredvoter.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_registeredvoter(self):
        registeredvoter = create_registeredvoter()
        data = {
            "name": "name",
            "public_index": "public_index",
            "authenticated_voter": create_authenticatedvoter().pk,
        }
        url = reverse('bulletin_registeredvoter_update',
                      args=[registeredvoter.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CredentialViewTest(unittest.TestCase):
    '''
    Tests for Credential
    '''

    def setUp(self):
        self.client = Client()

    def test_list_credential(self):
        url = reverse('bulletin_credential_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_credential(self):
        url = reverse('bulletin_credential_create')
        data = {
            "name": "name",
            "encrypted_credential": "encrypted_credential",
            "registered_voter": create_registeredvoter().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_credential(self):
        credential = create_credential()
        url = reverse('bulletin_credential_detail', args=[credential.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_credential(self):
        credential = create_credential()
        data = {
            "name": "name",
            "encrypted_credential": "encrypted_credential",
            "registered_voter": create_registeredvoter().pk,
        }
        url = reverse('bulletin_credential_update', args=[credential.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CredentialShareViewTest(unittest.TestCase):
    '''
    Tests for CredentialShare
    '''

    def setUp(self):
        self.client = Client()

    def test_list_credentialshare(self):
        url = reverse('bulletin_credentialshare_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_credentialshare(self):
        url = reverse('bulletin_credentialshare_create')
        data = {
            "name": "name",
            "encrypted_credential_share": "encrypted_credential_share",
            "registration_teller": create_registrationteller().pk,
            "credential": create_credential().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_credentialshare(self):
        credentialshare = create_credentialshare()
        url = reverse('bulletin_credentialshare_detail',
                      args=[credentialshare.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_credentialshare(self):
        credentialshare = create_credentialshare()
        data = {
            "name": "name",
            "encrypted_credential_share": "encrypted_credential_share",
            "registration_teller": create_registrationteller().pk,
            "credential": create_credential().pk,
        }
        url = reverse('bulletin_credentialshare_update',
                      args=[credentialshare.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RegistrationTellerViewTest(unittest.TestCase):
    '''
    Tests for RegistrationTeller
    '''

    def setUp(self):
        self.client = Client()

    def test_list_registrationteller(self):
        url = reverse('bulletin_registrationteller_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_registrationteller(self):
        url = reverse('bulletin_registrationteller_create')
        data = {
            "name": "name",
            "election": create_election().pk,
            "user": create_django_contrib_auth_models_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_registrationteller(self):
        registrationteller = create_registrationteller()
        url = reverse('bulletin_registrationteller_detail',
                      args=[registrationteller.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_registrationteller(self):
        registrationteller = create_registrationteller()
        data = {
            "name": "name",
            "election": create_election().pk,
            "user": create_django_contrib_auth_models_user().pk,
        }
        url = reverse('bulletin_registrationteller_update',
                      args=[registrationteller.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CastVoteViewTest(unittest.TestCase):
    '''
    Tests for CastVote
    '''

    def setUp(self):
        self.client = Client()

    def test_list_castvote(self):
        url = reverse('bulletin_castvote_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_castvote(self):
        url = reverse('bulletin_castvote_create')
        data = {
            "name": "name",
            "encrypted_credential": "encrypted_credential",
            "encrypted_choices": "encrypted_choices",
            "encrypted_index": "encrypted_index",
            "proof_credential": "proof_credential",
            "proof_choices": "proof_choices",
            "proof_index": "proof_index",
            "election": create_election().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_castvote(self):
        castvote = create_castvote()
        url = reverse('bulletin_castvote_detail', args=[castvote.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_castvote(self):
        castvote = create_castvote()
        data = {
            "name": "name",
            "encrypted_credential": "encrypted_credential",
            "encrypted_choices": "encrypted_choices",
            "encrypted_index": "encrypted_index",
            "proof_credential": "proof_credential",
            "proof_choices": "proof_choices",
            "proof_index": "proof_index",
            "election": create_election().pk,
        }
        url = reverse('bulletin_castvote_update', args=[castvote.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TabulationTellerViewTest(unittest.TestCase):
    '''
    Tests for TabulationTeller
    '''

    def setUp(self):
        self.client = Client()

    def test_list_tabulationteller(self):
        url = reverse('bulletin_tabulationteller_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_tabulationteller(self):
        url = reverse('bulletin_tabulationteller_create')
        data = {
            "name": "name",
            "public_key_share": "public_key_share",
            "election": create_election().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_tabulationteller(self):
        tabulationteller = create_tabulationteller()
        url = reverse('bulletin_tabulationteller_detail',
                      args=[tabulationteller.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_tabulationteller(self):
        tabulationteller = create_tabulationteller()
        data = {
            "name": "name",
            "public_key_share": "public_key_share",
            "election": create_election().pk,
        }
        url = reverse('bulletin_tabulationteller_update',
                      args=[tabulationteller.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class FinalTallyViewTest(unittest.TestCase):
    '''
    Tests for FinalTally
    '''

    def setUp(self):
        self.client = Client()

    def test_list_finaltally(self):
        url = reverse('bulletin_finaltally_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_finaltally(self):
        url = reverse('bulletin_finaltally_create')
        data = {
            "name": "name",
            "choice_tally": "choice_tally",
            "proof_tally": "proof_tally",
            "election": create_election().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_finaltally(self):
        finaltally = create_finaltally()
        url = reverse('bulletin_finaltally_detail', args=[finaltally.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_finaltally(self):
        finaltally = create_finaltally()
        data = {
            "name": "name",
            "choice_tally": "choice_tally",
            "proof_tally": "proof_tally",
            "election": create_election().pk,
        }
        url = reverse('bulletin_finaltally_update', args=[finaltally.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class QuestionViewTest(unittest.TestCase):
    '''
    Tests for Question
    '''

    def setUp(self):
        self.client = Client()

    def test_list_question(self):
        url = reverse('bulletin_question_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_question(self):
        url = reverse('bulletin_question_create')
        data = {
            "name": "name",
            "text": "text",
            "description": "description",
            "max_choices": "max_choices",
            "election": create_election().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_question(self):
        question = create_question()
        url = reverse('bulletin_question_detail', args=[question.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_question(self):
        question = create_question()
        data = {
            "name": "name",
            "text": "text",
            "description": "description",
            "max_choices": "max_choices",
            "election": create_election().pk,
        }
        url = reverse('bulletin_question_update', args=[question.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ChoiceViewTest(unittest.TestCase):
    '''
    Tests for Choice
    '''

    def setUp(self):
        self.client = Client()

    def test_list_choice(self):
        url = reverse('bulletin_choice_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_choice(self):
        url = reverse('bulletin_choice_create')
        data = {
            "name": "name",
            "text": "text",
            "votes": "votes",
            "question": create_question().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_choice(self):
        choice = create_choice()
        url = reverse('bulletin_choice_detail', args=[choice.slug, ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_choice(self):
        choice = create_choice()
        data = {
            "name": "name",
            "text": "text",
            "votes": "votes",
            "question": create_question().pk,
        }
        url = reverse('bulletin_choice_update', args=[choice.slug, ])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
