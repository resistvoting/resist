from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.core import serializers
from django.forms.models import model_to_dict
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User


from bulletin.models import Election, Question, Choice, EligibleVoter, AuthenticatedVoter, RegisteredVoter, RegistrationTeller, TabulationTeller, Credential, CredentialShare, CastVote

import requests, json
from Crypto.Random import random
from crypto import ElGamalCrypto as crypto

def is_member(user):
    return user.groups.filter(name='Voter').exists()

def build_question(question):
    choices = list(Choice.objects.filter(question = question).values('id', 'text'))[::-1]
    for choice in range(len(choices)):
        choices[choice]['selection'] = 0
    question_dict = {
        "questionID" : question.id,
        "questionText" : question.text,
        "questionDescription" : question.description,
        "questionMaxChoices" : question.max_choices,
        "numberOfSelectedChoices" : 0,
        "choices" : choices,
    }
    return question_dict

# Create your views here.

def electionactions(request):
    return render(request, 'booth/electionactions.html', {'electionID' : request.POST['electionID']})
    #context_object_name  = ''


def activeelection(request):
    return render(request, 'booth/index.html', {
        'elections' : Election.objects.filter(registration_frozen = True, voting_frozen = False),
   })

@login_required
@user_passes_test(is_member)
def registerindex(request):
    return render(request, 'booth/registerelectionlist.html', {
            'elections' : Election.objects.all()
        })

"""
    The register function helps voters obtain credential shares and build their credential.
    credential = credential_share_value[0] * credential_share_value[1] ...
    The booth initially checks if the voter has already been registered in the past for this election.
    If not, an entry is made in the database.
    A GET request is sent to the register app, where a new credential share is generated and the credential value 
    on the bulletin board gets updated.
    The response from each registration teller is a credential share, which is added to an array and multiplied with the 
    previous value of credential.
"""
@login_required
@user_passes_test(is_member)
def register(request):
    user = request.user
    election = Election.objects.get(election_uuid = request.POST['election_uuid'])
    election_public_key = crypto.PublicKey.from_json(election.public_key)
    eligible_voter = EligibleVoter.objects.get(user = user, election = election)
    authenticated_voter = AuthenticatedVoter.objects.get(eligible_voter = eligible_voter)
    registration_tellers = RegistrationTeller.objects.filter(election = election)
    try:
        registered_voter = RegisteredVoter.objects.get(authenticated_voter = authenticated_voter)
        credential = Credential(registered_voter = registered_voter)
        return render(request, 'booth/error.html', {
            'message' : "Credential Shares already generated for this user."
        })
    except:
        registered_voter = RegisteredVoter(authenticated_voter = authenticated_voter)
        registered_voter.save()
        credential = Credential(registered_voter = registered_voter, encrypted_credential = crypto.ElGamalCrypto.encrypt(message = 1, public_key = election_public_key).to_json())
        credential.save()
    credential_shares = []
    credential = 1
    for registration_teller in registration_tellers:
        try:
            credential_share = CredentialShare.objects.get(registration_teller = registration_teller, credential = credential)
            data = "Credential Share already generated."
        except:
            url = "http://" + request.get_host() + reverse('register:credentialshare', kwargs = {'election_uuid' : election.election_uuid, 'rid' : registration_teller.id, 'public_index' : authenticated_voter.public_index})
            response = requests.get(url)
            data = response.json()
            credential_share_value = data["credential_share"]
            credential = (credential * credential_share_value) % election_public_key.key_parameters.p
            credential_shares.append(credential_share_value)
    encrypted_credential = crypto.ElGamalCrypto.encrypt(message = credential, public_key = election_public_key)
    fake_credential_shares = crypto.generate_fake_credentials(credential_shares = credential_shares)
    fake_credentials = []
    encrypted_fake_credentials = []
    for i in range(0, len(fake_credential_shares)):
        temp = 1
        for j in range(0, len(fake_credential_shares[i])):
            temp = (temp * fake_credential_shares[i][j]) % election_public_key.key_parameters.p
        fake_credentials.append(temp)
        encrypted_fake_credentials.append(crypto.ElGamalCrypto.encrypt(message = fake_credentials[i], public_key = election_public_key))
    print("Encrypted Credential : ")
    print(encrypted_credential)
    public_index = int(authenticated_voter.public_index)
    encrypted_public_index = crypto.ElGamalCrypto.encrypt(message = public_index, public_key = election_public_key)
    downloadable_public_index = {
        "a" : str(encrypted_public_index.a),
        "b" : str(encrypted_public_index.b),
    }
    downloadable_encrypted_credential = {
        "a" : str(encrypted_credential.a),
        "b" : str(encrypted_credential.b),
    }
    downloadable_credential = {
        "credential_shares" : [str(i) for i in credential_shares],
        "credential" : str(credential),
        "encrypted_credential" : downloadable_encrypted_credential,
        "public_index" : downloadable_public_index,
    }
    final_downloadable_fake_credentials = []
    for i in range(0, len(fake_credentials)):
        temp = {
            "credential_shares" : [str(j) for j in fake_credential_shares[i]],
            "credential" : str(fake_credentials[i]),
            "encrypted_credential" : {
                "a" : str(encrypted_fake_credentials[i].a),
                "b" : str(encrypted_fake_credentials[i].b),
            },
            "public_index" : downloadable_public_index,

        }
        final_downloadable_fake_credentials.append(temp)
        
    return render(request, 'booth/credentialdownload.html', {
        'credential' : downloadable_credential,
        'fake_credentials' : final_downloadable_fake_credentials,
    })


def castvote(request):
    vote = json.loads(request.body)
    print(vote)
    election = Election.objects.get(election_uuid = vote['electionUUID'])
    cast_vote = CastVote(election = election, encrypted_credential = vote['encrypted_credential'], encrypted_choices = vote['encrypted_choices'], encrypted_index = vote['public_index'])
    cast_vote.proof_choices = [ {0 : 0} ]
    cast_vote.proof_credential = {0 : 0}
    cast_vote.proof_index = {0 : 0}
    cast_vote.save()
    return HttpResponse(reverse('booth:index'))


def vote(request):
    election = Election.objects.get(election_uuid = request.POST['election_id'])
    downloadable_encrypted_choices = []
    for encrypted_choice in election.encrypted_choices:
        encrypted_choice = json.loads(encrypted_choice)
        temp = {
            "a" : str(encrypted_choice["a"]),
            "b" : str(encrypted_choice["b"]),
        }
        downloadable_encrypted_choices.append(temp)
    public_key = json.loads(election.public_key)
    election_metadata = {
        "electionUUID": str(election.election_uuid),
        "electionName" : election.name,
        "electionOrganization" : election.organization,
        "electionDescription" : election.description,
        "electionPublicKey" : {
            "p" : str(public_key["p"]),
            "q" : str(public_key["q"]),
            "g" : str(public_key["g"]),
            "y" : str(public_key["y"]),
        },
        "electionHelpEmail" : election.help_email,
        "electionCastUrl" : reverse('booth:castvote'),
        "encrypted_choices" : downloadable_encrypted_choices,
    }
    questions_obj = Question.objects.filter(election = election)
    questions_list = [ build_question(question) for question in questions_obj][::-1]
    election_metadata['numberOfQuestions'] = len(questions_list)
    ballot = {
        "electionMetadata" : election_metadata,
        "questions" : questions_list,
    }
    election.save()
    return render(request, 'booth/vote.html', {
        'ballot': ballot,
    })