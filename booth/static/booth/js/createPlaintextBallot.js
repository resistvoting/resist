function PlaintextBallot(ballot) {
    console.log(ballot);
    plaintextBallot = {
        "electionID" : ballot.electionMetadata.electionUUID,
    };
    questions = [{}];
    questionIndex = 0;
    ballot.questions.forEach(ballotQuestion => {
        question = {
            "questionID" : ballotQuestion.questionID,
            "choices" : [{}],
        }
        choiceIndex = 0;
        ballotQuestion.choices.forEach(choice => {
            question.choices[choiceIndex++] = {
                "choiceID" : choice.id,
                "choice" : 0,
            }

        });  
        questions[questionIndex] = question;
        questionIndex++;      
    });
    plaintextBallot.questions = questions;
}