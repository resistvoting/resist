from ElGamalCrypto import (
    ElGamalCrypto,
    ElGamalKey,
    ElGamalKeyParameters,
    EncryptedMessage,
    NMEncryptedMessage
)

keyParameters = ElGamalCrypto.static_512_key_parameters()
key = ElGamalCrypto.generateKey(512, keyParameters)
messageToEncrypt = 4
encodedMessage = ElGamalCrypto.encode(messageToEncrypt, keyParameters)
encryptedMessage = ElGamalCrypto.nmencrypt(message = messageToEncrypt, publicKey = key.y, params = keyParameters)
print("Public Key: " + str(key.y) + "\nPrivate Key: " + str(key.x) + "\n")
print("Plaintext Message: " + str(messageToEncrypt) + "\nEncrypted Message:\n-a: " +
      str(encryptedMessage.a) + "\n-b: " + str(encryptedMessage.b) + "\n-c: " + str(encryptedMessage.c) + "\n-d: " + str(encryptedMessage.d) + "\n")
decryptedMessage = ElGamalCrypto.decrypt(encryptedMessage, key.x, keyParameters)
print("Decrypted Message: " + str(decryptedMessage))
print("Encoded Message: " + str(encodedMessage))
if (decryptedMessage == encodedMessage) : 
    print("They are equal")