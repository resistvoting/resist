from django.urls import reverse
from django_extensions.db.fields import AutoSlugField
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.fields import JSONField
from django.db.models import BooleanField
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import EmailField
from django.db.models import IntegerField
from django.db.models import TextField
from django.db.models import UUIDField
from django_extensions.db.fields import AutoSlugField
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db import models as models
from django_extensions.db import fields as extension_fields

import uuid, json

from crypto import ElGamalCrypto as crypto

from users.models import User as AuthenticatedVoterPrivate


class Election(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    election_uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    organization = models.CharField(max_length=30)
    description = models.TextField(max_length=100)
    public_key = JSONField(default=dict, null=True)
    private_key = JSONField(default=dict, null=True)
    key_commitments = ArrayField(JSONField(), null=True)
    encrypted_choices = ArrayField(JSONField(), null=True)
    max_tellers = models.IntegerField(default=3)
    threshold_tellers = models.IntegerField(default=2)
    registration_frozen = models.BooleanField(default=False)
    voting_frozen = models.BooleanField(default=False)
    results_released = models.BooleanField(default=False)
    help_email = models.EmailField()

    # Relationship Fields
    supervisor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE, related_name="supervisor",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_election_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_election_update', args=(self.slug,))
    
    def __str__(self):
        return self.name


class EligibleVoter(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='user', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    # Relationship Fields
    election = models.ManyToManyField(
        'bulletin.Election',
        related_name="eligiblevoters",
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE, related_name="eligiblevoters",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_eligiblevoter_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_eligiblevoter_update', args=(self.slug,))
    
    def __str__(self):
        return self.user.username


class AuthenticatedVoter(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='eligible_voter', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    authentication_key_public = JSONField(default=dict, blank=True)
    designation_key_public = JSONField(default=dict, blank=True)
    public_index = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    # Relationship Fields
    eligible_voter = models.ForeignKey(
        'bulletin.EligibleVoter',
        on_delete=models.CASCADE, related_name="authenticatedvoters",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_authenticatedvoter_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_authenticatedvoter_update', args=(self.slug,))
    
    def __str__(self):
        return self.eligible_voter.user.username

    def save(self, *args, **kwargs):
        eligible_voter_user = self.eligible_voter.user.username
        authentication_key = crypto.ElGamalCrypto.generate_key(
            512)
        designation_key = crypto.ElGamalCrypto.generate_key(
            512)
        voterprivate = AuthenticatedVoterPrivate.objects.get(username = eligible_voter_user)
        voterprivate.authentication_key_private = crypto.PrivateKey(x = authentication_key.x, key_params = authentication_key.key_parameters).to_json()
        voterprivate.designation_key_private = crypto.PrivateKey(x = designation_key.x, key_params = designation_key.key_parameters).to_json()
        self.authentication_key_public = crypto.PublicKey(y = authentication_key.y, key_params = authentication_key.key_parameters).to_json()
        self.designation_key_public = crypto.PublicKey(y = designation_key.y, key_params = designation_key.key_parameters).to_json()
        voterprivate.save()
        super(AuthenticatedVoter, self).save(*args, **kwargs)



class RegisteredVoter(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='authenticated_voter', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    # Relationship Fields
    authenticated_voter = models.ForeignKey(
        'bulletin.AuthenticatedVoter',
        on_delete=models.CASCADE, related_name="registeredvoters",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_registeredvoter_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_registeredvoter_update', args=(self.slug,))
    
    def __str__(self):
        return self.authenticated_voter.eligible_voter.user.username


class Credential(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='registered_voter', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    encrypted_credential = JSONField(default=dict)

    # Relationship Fields
    registered_voter = models.ForeignKey(
        'bulletin.RegisteredVoter',
        on_delete=models.CASCADE, related_name="credentials",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_credential_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_credential_update', args=(self.slug,))
    
    def __str__(self):
        return json.dumps(self.encrypted_credential)


class CredentialShare(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='credential', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    encrypted_credential_share = JSONField(default=dict)

    # Relationship Fields
    registration_teller = models.ForeignKey(
        'bulletin.RegistrationTeller',
        on_delete=models.CASCADE, related_name="credentialshares",
    )
    credential = models.ForeignKey(
        'bulletin.Credential',
        on_delete=models.CASCADE, related_name="credentialshares",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_credentialshare_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_credentialshare_update', args=(self.slug,))
    
    def __str__(self):
        return json.dumps(self.encrypted_credential_share)


class RegistrationTeller(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='user', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    public_key = JSONField(default=dict, null=True)
    # Relationship Fields
    election = models.ForeignKey(
        'bulletin.Election',
        on_delete=models.CASCADE, related_name="registrationtellers",
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE, related_name="registrationtellers",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_registrationteller_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_registrationteller_update', args=(self.slug,))

    def __str__(self):
        return self.user.username
    

class CastVote(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='election', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    encrypted_credential = JSONField(default=dict)
    encrypted_choices = ArrayField(JSONField())
    encrypted_index = JSONField(default=dict)
    proof_credential = JSONField(default=dict)
    proof_choices = ArrayField(JSONField())
    proof_index = JSONField(default=dict)

    # Relationship Fields
    election = models.ForeignKey(
        'bulletin.Election',
        on_delete=models.CASCADE, related_name="castvotes",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_castvote_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_castvote_update', args=(self.slug,))
    


class TabulationTeller(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='user', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    public_key_share = JSONField(default=dict)

    # Relationship Fields
    election = models.ForeignKey(
        'bulletin.Election',
        on_delete=models.CASCADE, related_name="tabulationtellers",
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE, related_name="tabulationtellers",
    )


    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_tabulationteller_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_tabulationteller_update', args=(self.slug,))
    
    def __str__(self):
        return self.user.username


class FinalTally(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='election', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    choice_tally = ArrayField(models.IntegerField(default=0))
    proof_tally = ArrayField(JSONField())

    # Relationship Fields
    election = models.ForeignKey(
        'bulletin.Election',
        on_delete=models.CASCADE, related_name="finaltallys",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_finaltally_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_finaltally_update', args=(self.slug,))


class Question(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='election', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    text = models.CharField(max_length=200)
    description = models.TextField(max_length=500)
    max_choices = models.IntegerField(default=1)

    # Relationship Fields
    election = models.ForeignKey(
        'bulletin.Election',
        on_delete=models.CASCADE, related_name="questions",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_question_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_question_update', args=(self.slug,))

    def __str__(self):
        return self.text

class Choice(models.Model):

    # Fields
    slug = extension_fields.AutoSlugField(populate_from='question', blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    # Relationship Fields
    question = models.ForeignKey(
        'bulletin.Question',
        on_delete=models.CASCADE, related_name="choices",
    )

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.slug

    def get_absolute_url(self):
        return reverse('bulletin_choice_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('bulletin_choice_update', args=(self.slug,))
    
    def __str__(self):
        return self.text
