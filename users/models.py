from django.db import models
from django.contrib.auth.models import AbstractUser

from django.contrib.postgres.fields import JSONField

# Create your models here.
class User(AbstractUser):
    authentication_key_private = JSONField(default=dict, null=True)
    designation_key_private = JSONField(default=dict, null=True)
    pass
