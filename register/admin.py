from django.contrib import admin
from django import forms
from .models import RegistrationTellerPrivate

# Register your models here.
class RegistrationTellerPrivateForm(forms.ModelForm):

    class Meta:
        model = RegistrationTellerPrivate
        fields = '__all__'

class RegistrationTellerPrivateAdmin(admin.ModelAdmin):
    form = RegistrationTellerPrivateForm
    list_display = ['created', 'last_updated', 'election',
                    'user', 'private_key']
    readonly_fields = ['created', 'last_updated', 'election',
                       'user', 'private_key']

admin.site.register(RegistrationTellerPrivate, RegistrationTellerPrivateAdmin)