from threshold_crypto import (ThresholdCrypto, ThresholdParameters)

# Generate parameters, public key and shares
key_params = ThresholdCrypto.generate_key_parameters(2048)
thresh_params = ThresholdParameters(3, 5)
pub_key, key_shares = ThresholdCrypto.create_public_key_and_shares_centralized(
    key_params, thresh_params)
print (key_params)
print (thresh_params)
print (pub_key)
print (key_shares)
