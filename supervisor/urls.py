from django.urls import path

from . import views

app_name = 'supervisor'
urlpatterns = [
    #supervisor urls
    path('index/',views.index, name='index'),
    path('createelection/', views.createelection, name='createelection'),
    path('electionlist/', views.electionlist, name='electionlist'),
    path('electionactions/', views.electionactions, name='electionactions'),
    path('generatepublickey', views.generate_public_key, name='generatepublickey'),
    path('registrationkeys', views.registration_keys, name='registrationkeys'),
    path('freezeregistration/', views.freezeregistration, name='freezeregistration'),
    path('freezevoting/', views.freezevoting, name='freezevoting'),
    path('releaseresults/', views.releaseresults, name='releaseresults'),
    path('addelection/', views.addelection, name='addelection'),
    path('createtabulation/', views.createtabulation, name='createtabulation'),
    path('createauth/', views.createvoter, name='createvoter'),
    path('createvoter/', views.createvoter, name='createvoter'),
    path('regvoter/', views.regvoter, name='register'),
]
