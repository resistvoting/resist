import nacl.utils
import nacl.secret
import nacl.encoding
import nacl.exceptions
import nacl.hash

import json
import number
from ElGamalCrypto import (
    ElGamalCrypto,
    ElGamalKey,
    ElGamalKeyParameters,
    EncryptedMessage,
    NMEncryptedMessage
)


class EncryptedCredential:
    """
    A non-malleable encrypted credential in the scheme. Because a hybrid approach is used it consists of four parts:
    - a = g^r mod p as in the ElGamal scheme
    - b = m * g^r mod p as in the ElGamal scheme with m being the value to be encrypted
    - c = hash(g^t mod p, a, b, rid, vid) mod q
    - d = (t + c*r) mod q
    The symmetric key is derived from the ElGamal encrypted value m.
    """
    @staticmethod
    def from_json(json_str: str):
        obj = json.loads(json_str)
        return EncryptedCredential.from_dict(obj)

    @staticmethod
    def from_dict(obj: dict):
        return EncryptedCredential(obj['a'], obj['b'], obj['c'], obj['d'])

    def __init__(self, a: int, b: int, c: int, d: int):
        """
        Construct a encrypted credential.

        :param a: like in ElGamal scheme
        :param b: like in ElGamal scheme
        :param c: like as a Hash
        :param d: evaluated value
        """
        self._a = a
        self._b = b
        self._c = c
        self._d = d

    @property
    def a(self) -> int:
        return self._a

    @property
    def b(self) -> int:
        return self._b

    @property
    def c(self) -> int:
        return self._c

    @property
    def d(self) -> int:
        return self._d

    def to_dict(self):
        return {
            'a': self.a,
            'b': self.b,
            'c': self.c,
            'd': self.d,
        }

    def to_json(self):
        return json.dumps(self.to_dict())

    def __eq__(self, other):
        return (isinstance(other, self.__class__) and
                self.a == other.a and
                self.b == other.b and
                self.c == other.c and
                self.d == other.d)

    def __str__(self):
        return 'EncryptedCredential:\n\ta = %d\n\tb = %d\n\tc = %d\n\td = %d\n' % (self._a, self._b, self._c, self._d)



class CredentialCrypto :
    @staticmethod
    def encrypt(credentialShare: int, publicKey: int, params: ElGamalKeyParameters, randomizationFactor: int, rid: int, vid: int) -> EncryptedCredential:
        t = number.getRandomRange(1, params.q)
        encryptedMessage = ElGamalCrypto.encrypt(
            credentialShare, publicKey, params, randomizationFactor, True)
        valueToHash = {
            'first': pow(params.g, t, params.p),
            'second': encryptedMessage.a,
            'third': encryptedMessage.b,
            'fourth': rid,
            'fifth': vid,
        }
        c = int.from_bytes(nacl.hash.blake2b(bytes(json.dumps(valueToHash), 'utf-8'),
                                             encoder=nacl.encoding.Base64Encoder), 'big') % params.q
        d = (t + (c * randomizationFactor)) % params.q
        return EncryptedCredential(encryptedMessage.a, encryptedMessage.b, c, d)
    
    def credVer(encryptedCredentialShare: EncryptedCredential, params: ElGamalKeyParameters, rid: int, vid: int):
        valueToHash = {
            'first': (pow(params.g, encryptedCredentialShare.d, params.p) * pow(pow(encryptedCredentialShare.a, encryptedCredentialShare.c, params.p), params.p - 2, params.p)) % params.p,
            'second': encryptedCredentialShare.a,
            'third': encryptedCredentialShare.b,
            'fourth': rid,
            'fifth': vid,
        }
        verification = int.from_bytes(nacl.hash.blake2b(bytes(json.dumps(valueToHash), 'utf-8'),
                                                        encoder=nacl.encoding.Base64Encoder), 'big') % params.q
        if (verification == encryptedCredentialShare.c):
            print("Successfully Verified")
