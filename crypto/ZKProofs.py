import nacl.utils
import nacl.secret
import nacl.encoding
import nacl.exceptions
import nacl.hash

import json
import crypto.number
from crypto.ElGamalCrypto import (
    ElGamalCrypto,
    ElGamalKey,
    ElGamalKeyParameters,
    EncryptedMessage,
    NMEncryptedMessage
)

class ZKProof:
    @staticmethod
    def ProveKnowDLog(h: int, v: int, params: ElGamalKeyParameters):
        """
        Due to: Schnorr 
        Principals: Prover P and Verifier V
        Public Input: h,v
        Private Input: x, such that, v≡h^x (mod p)
        """
        z = number.getRandomRange(1, params.q)
        a = pow(h, z, p)
        valueToHash = {
            'first' : v,
            'second' : a,
        }