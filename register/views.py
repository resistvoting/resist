from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, JsonResponse
from django.core import serializers
import json
from bulletin.models import Election, RegistrationTeller, AuthenticatedVoter, RegisteredVoter, Credential, CredentialShare
from register.models import RegistrationTellerPrivate
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test


from crypto import ElGamalCrypto as crypto
from Crypto.Random import random

def is_member(user):
    return user.groups.filter(name='Voter').exists()

# Create your views here.
def start(request, election_id):
    election = get_object_or_404(Election, pk = election_id)
    registration_tellers = election.registrationteller_set.values(
        'uuid', 'name', 'registration_public_key')
    return render(request, 'register/start.html', {
        'election' : election,
        'registrationTellers' : registration_tellers,
    })
#Needham-Schroeder-Lowe
def credential_share(request, election_uuid, rid, public_index):
    print("Found reg teller")
    election = Election.objects.get(election_uuid = election_uuid)
    election_public_key = crypto.PublicKey.from_json(election.public_key)
    registration_teller = RegistrationTeller.objects.get(id = rid)
    authenticated_voter = AuthenticatedVoter.objects.get(public_index = public_index)
    registered_voter = RegisteredVoter.objects.get(authenticated_voter = authenticated_voter)
    credential = Credential.objects.get(registered_voter = registered_voter)
    credential_share_value = random.getrandbits(512)
    encrypted_credential_share = crypto.ElGamalCrypto.credential_encrypt(credential_share = credential_share_value, public_key = election_public_key, rid = str(rid), vid = str(public_index))
    credential_share = CredentialShare(registration_teller = registration_teller, credential = credential, encrypted_credential_share = encrypted_credential_share.to_json())
    old_credential = json.loads(credential.encrypted_credential)
    print(old_credential)
    old_a = old_credential["a"]
    old_b = old_credential["b"]
    new_a = (int(old_a) * int(encrypted_credential_share.a)) % int(election_public_key.key_parameters.p)
    new_b = (int(old_b) * int(encrypted_credential_share.a)) % int(election_public_key.key_parameters.p)
    new_credential_value = {
        "a" : int(new_a),
        "b" : int(new_b),
    }
    credential.encrypted_credential = json.dumps(new_credential_value)
    credential.save()
    credential_share.save()
    response = JsonResponse({
        "credential_share" : credential_share_value,
    })
    #print(crypto.EncryptedCredential(encrypted_credential_share).to_json())
    return response