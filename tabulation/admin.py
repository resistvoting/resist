from django.contrib import admin
from django import forms
from .models import TabulationTellerPrivate

# Register your models here.
class TabulationTellerPrivateForm(forms.ModelForm):

    class Meta:
        model = TabulationTellerPrivate
        fields = '__all__'

class TabulationTellerPrivateAdmin(admin.ModelAdmin):
    form = TabulationTellerPrivateForm
    list_display = ['created', 'last_updated', 'election',
                    'user', 'key_share', 'z']
    readonly_fields = ['created', 'last_updated', 'election',
                       'user', 'key_share', 'z']

admin.site.register(TabulationTellerPrivate, TabulationTellerPrivateAdmin)