
function encryptBallot(ballot) {
    electionPublicKey = bigInt(ballot.electionMetadata.electionPublicKey);
    elGamalParameters = {
      p: bigInt(ballot.electionMetadata.electionKeyParametersP),
      q: bigInt(ballot.electionMetadata.electionKeyParametersQ),
      g: bigInt(ballot.electionMetadata.electionKeyParametersG)
    };
    ballot.questions.forEach( function(question, questionindex) {
        question.choices.forEach( function (choice, choiceindex) {
            ballot.questions[questionindex].choices[choiceindex].selection = elGamalEncrypt(choice.selection, electionPublicKey, elGamalParameters);
        });
    });
    console.log("Encrypted Ballot : ");
    console.log(ballot);
    return ballot;
}