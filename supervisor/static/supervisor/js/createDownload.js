function createDownload(obj) {
    var data =
      "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(obj));

    var a = document.getElementById("publicKeyDownload");
    a.href = "data:" + data;
    a.download = "data.json";
    a.innerHTML = "Download Public Key";
}