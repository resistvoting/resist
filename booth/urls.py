"""resist URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from . import views

app_name = 'booth'
urlpatterns = [
    path('', views.activeelection, name='index'),
    path('registerindex', views.registerindex, name='registerindex'),
    path('register', views.register, name='register'),
    path('electionactions', views.electionactions, name='electionactions'),
    path('castvote', views.castvote, name='castvote'),
    path('vote', views.vote, name='vote'),
]
