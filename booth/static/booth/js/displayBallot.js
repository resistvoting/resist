function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === name + "=") {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}
var csrftoken = getCookie("csrftoken");

function castEncryptedBallot(ballot) {
  const Http = new XMLHttpRequest();
  const url = ballot.electionCastUrl;
  Http.open("POST", url);
  Http.setRequestHeader("Content-type", "application/json; charset=utf-8");
  Http.setRequestHeader(
    "X-CSRFToken",
    csrftoken
  );
  Http.send(JSON.stringify(ballot));
  Http.onreadystatechange = function() {
    if(this.onreadystatechange > 3 || this.status == 200) {
      window.location.href = Http.responseText;
    }
  }
}

function ReviewBallot(ballot) {
  let description = document.getElementById("description");
  description.innerHTML = "<p>Encrypted_Ballot</p><br /><br /><textarea id='result'></textarea>";
  document.getElementById('result').value = JSON.stringify(ballot, null, 2);
  document.getElementById('button-cast').value = "Cast Vote";
  document.getElementById('button-cast').classList.add('green');
  document.getElementById('button-cast').onclick = function() {
    castEncryptedBallot(ballot);
  };
}


function addCredentials(ballot) {
  let description = document.getElementById("description");
  var result = {}
  description.innerHTML = "<input type='file' id='selectFiles' value='Import' /><br /><br /><button id='import'>Import</button><br /><br /><textarea id='result'></textarea>";
  document.getElementById('button-cast').value = "Credential Not Uploaded";
  document.getElementById('button-cast').addEventListener("click", function() {});
  document.getElementById('button-cast').classList.remove('green');
  document.getElementById('import').onclick = function() {
    var files = document.getElementById('selectFiles').files;
    console.log(files);
    if (files.length <= 0) {
      return false;
    }
    
    var fr = new FileReader();
    
    fr.onload = function(e) { 
    console.log(e);
      result = JSON.parse(e.target.result);
      var formatted = JSON.stringify(result, null, 2);
      document.getElementById('result').value = formatted;
      document.getElementById('button-cast').value = "Review Ballot";
      document.getElementById('button-cast').classList.add('green');
      document.getElementById('button-cast').addEventListener("click", function (){
        ballot.public_index =  result.public_index;
        ballot.encrypted_credential = result.encrypted_credential;
        ReviewBallot(ballot);
      })
    }

    fr.readAsText(files.item(0));

  };

}

function unSelectChoice(ballot, currentQuestionNumber, questionID, choiceid) {
  if (
    ballot.questions[currentQuestionNumber].numberOfSelectedChoices > 0
  ) {
    ballot.questions[currentQuestionNumber].choices.forEach(choice => {
      if (choice.id == choiceid && choice.selection == 1) {
        choice["selection"] = 0;
        if ((selectedChoice = document.getElementById("choice-" + choiceid))) {
          selectedChoice.classList.remove("active");
        }
        ballot.questions[currentQuestionNumber].numberOfSelectedChoices--;
        return ballot;
      }
    });
  }
  return ballot;
}

function selectChoice(ballot, currentQuestionNumber, questionID, choiceid) {
    if (
      ballot.questions[currentQuestionNumber].numberOfSelectedChoices >=
      ballot.questions[currentQuestionNumber].questionMaxChoices
    )   {
        alert("Uncheck the one you don't want. Then choose the one you want.");
    }
    else {
        ballot.questions[currentQuestionNumber].choices.forEach ( choice => {
            if (choice.id == choiceid) {
                choice['selection'] = 1;
                if (
                (selectedChoice = document.getElementById(
                    "choice-" + choiceid
                ))
                ) {
                selectedChoice.classList.add("active");
                }
                ballot.questions[currentQuestionNumber].numberOfSelectedChoices++;
                return ballot;
            }
        });
    }
    return ballot;
}

function displayQuestion(ballot, currentQuestionNumber) {
    console.log("Current Question: " + currentQuestionNumber);
    maxQuestions = ballot.electionMetadata.numberOfQuestions;
    let description = document.getElementById("description");
    //QuestionTitle
    let QuestionTitle = document.createElement("H1");
    QuestionTitle.classList.add("info");
    QuestionTitle.classList.add("open-primary")
    let strongQuestionText = document.createElement("strong");
    strongQuestionText.innerHTML = ballot.questions[currentQuestionNumber].questionText;
    QuestionTitle.appendChild(strongQuestionText);
    //QuestionConstraints
    let maxChoicesValue = ballot.questions[currentQuestionNumber].questionMaxChoices;
    let maxChoicesHeader = document.createElement("H3");
    maxChoicesHeader.classList.add('open-secondary');
    let maxChoicesStrongText = document.createElement("STRONG");
    maxChoicesStrongText.innerHTML = "Vote for " + maxChoicesValue + " Choices.";
    maxChoicesHeader.appendChild(maxChoicesStrongText);
    //Choices
    optionsDiv = document.createElement("div");
    optionsDiv.id = "options";
    optionsDiv.classList.add('scroll', 'bottom', 'override', 'top');
    optionsDiv.style.height = "auto";
    ballot.questions[currentQuestionNumber].choices.forEach(choice => {
        let optionDiv = document.createElement('div');
        optionDiv.classList.add('option');
        optionDiv.addEventListener('click', function () {
            if (document.getElementById("choice-" + choice.id).classList.contains("active")) {
                ballot = unSelectChoice(
                    ballot,
                    currentQuestionNumber,
                    ballot.questions[
                    currentQuestionNumber
                    ].questionID,
                    choice.id
                );

            }
            else {
                ballot = selectChoice(
                    ballot,
                    currentQuestionNumber,
                    ballot.questions[
                    currentQuestionNumber
                    ].questionID,
                    choice.id
                );

            }
        });
        let optionDesc = document.createElement('div');
        optionDesc.id = "choice-" + choice.id;
        optionDesc.classList.add('choice');
        if(choice.selection == 1) {
          optionDesc.classList.add("active");
        }
        let tickBox = document.createElement('div');
        tickBox.classList.add('vote');
        let choiceDesc = document.createElement('div');
        choiceDesc.classList.add('desc');
        let choiceText = document.createElement('p');
        choiceText.classList.add('candidate');
        choiceText.innerHTML = choice.text;
        choiceDesc.appendChild(choiceText);
        optionDesc.appendChild(tickBox);
        optionDesc.appendChild(choiceDesc);
        optionDiv.appendChild(optionDesc);
        optionsDiv.appendChild(optionDiv);
    });
    description.innerHTML = "";
    description.appendChild(QuestionTitle);
    description.appendChild(maxChoicesHeader);
    description.appendChild(optionsDiv);
}

function displayEncryptedBallot(ballot, encrypted_choices) {
  document.getElementById('description').innerHTML = "";
  document.getElementById('button-back').style.display = null;
  document.getElementById('button-cast').value = "Proceed to Add Credentials";
  document.getElementById('button-cast').addEventListener("click", function() {
    encrypted_ballot = {
      electionUUID : ballot.electionMetadata.electionUUID,
      electionCastUrl : ballot.electionMetadata.electionCastUrl,
      encrypted_choices : encrypted_choices,
    };
    addCredentials(encrypted_ballot);
  });
  //header 1
  let header1 = document.createElement("H1");
  header1.classList.add("info");
  header1.classList.add("open-review");
  header1.innerHTML = "<strong>Review what you're voting for</strong>";

  let optionsDiv = document.createElement("div");
  optionsDiv.classList.add("scroll");
  optionsDiv.classList.add("override");
  optionsDiv.style.height = "auto";
  i = 0;
  ballot.questions.forEach( function (question, index) {
    let reviewDiv = document.createElement("div");
    reviewDiv.classList.add("review");
    let questionTitle = document.createElement("H2");
    questionTitle.innerHTML = "<strong>" + question.questionText + "</strong>";
    reviewDiv.appendChild(questionTitle);
    //encrypted choices
    question.choices.forEach( choice => {
      let selectedOptionDiv = document.createElement("div");
      selectedOptionDiv.classList.add("option");
      let optionDesc = document.createElement("div");
      optionDesc.id = "choice-" + choice.id;
      let choiceDesc = document.createElement("div");
      choiceDesc.classList.add("desc");
      let choiceText1 = document.createElement("p");
      choiceText1.classList.add("candidate");
      choiceText1.innerHTML = encrypted_choices[i].a;
      let choiceText2 = document.createElement("p");
      choiceText2.classList.add("candidate");
      choiceText2.innerHTML = encrypted_choices[i].b;
      choiceDesc.appendChild(choiceText1);
      choiceDesc.appendChild(choiceText2);
      optionDesc.appendChild(choiceDesc);
      selectedOptionDiv.appendChild(optionDesc);
      reviewDiv.appendChild(selectedOptionDiv);
      i++;
    });
    optionsDiv.appendChild(reviewDiv);
  });
  document.getElementById("description").appendChild(optionsDiv);
}

function displayReview(ballot, currentQuestionNumber) {
    //header1
    document.getElementById('description').innerHTML = "";
    let header1 = document.createElement("H1");
    header1.classList.add("info");
    header1.classList.add("open-review");
    header1.innerHTML = "<strong>Review what you're voting for</strong>";
    //header3
    let header3 = document.createElement("H3");
    header3.classList.add("open-review");
    header3.innerHTML = "<strong>This screen shows everything you voted for.</strong> Review it carefully. If you are ready to cast your ballot, touch <strong>Cast your Vote.</strong>";
    document.getElementById("description").appendChild(header1);
    document.getElementById("description").appendChild(header3);
 
    let optionsDiv = document.createElement("div");
    optionsDiv.classList.add("scroll");
    optionsDiv.classList.add("override");
    optionsDiv.style.height = "auto";
    ballot.questions.forEach( function (question, index) {
      let reviewDiv = document.createElement("div");
      reviewDiv.classList.add("review");
      let questionTitle = document.createElement("H2");
      questionTitle.innerHTML = "<strong>" + question.questionText + "</strong>";
      reviewDiv.appendChild(questionTitle);
      if(question.numberOfSelectedChoices == 0) {
        let warningDiv = document.createElement("div");
        warningDiv.classList.add("warning");
        warningDiv.addEventListener("click", function() {
          displayBallot(ballot, index);
        });
        warningDiv.innerHTML = "<p>You did not vote for anyone. <br> If you want to vote, touch here.</p>";
        reviewDiv.appendChild(warningDiv);
      } else {
        question.choices.forEach( choice => {
          if (choice.selection == 1) {
            let selectedOptionDiv = document.createElement("div");
            selectedOptionDiv.classList.add("option");
            selectedOptionDiv.addEventListener("click", function() {
              displayBallot(ballot, index);
            });
            let optionDesc = document.createElement("div");
            optionDesc.id = "choice-" + choice.id;
            optionDesc.classList.add("choice", "active");
            let tickBox = document.createElement("div");
            tickBox.classList.add("vote");
            let choiceDesc = document.createElement("div");
            choiceDesc.classList.add("desc");
            let choiceText = document.createElement("p");
            choiceText.classList.add("candidate");
            choiceText.innerHTML = choice.text;
            choiceDesc.appendChild(choiceText);
            optionDesc.appendChild(tickBox);
            optionDesc.appendChild(choiceDesc);
            selectedOptionDiv.appendChild(optionDesc);
            reviewDiv.appendChild(selectedOptionDiv);
          } 
        });
      }
      optionsDiv.appendChild(reviewDiv);
    });
    let endOfReview = document.createElement("H2");
    endOfReview.innerHTML = "End of review";
    optionsDiv.appendChild(endOfReview);
    document.getElementById("description").appendChild(optionsDiv);
    document.getElementById("button-next").style.display = null;
    document.getElementById("button-cast").style.display = "inline-block";
    document.getElementById("button-cast").value =
      "Encrypt your ballot";
    document.getElementById("button-cast").addEventListener("click", function() {
      encrypted_choices = encryptBallot(ballot);
      displayEncryptedBallot(ballot, encrypted_choices);
    });
}

function displayBallot(ballot, currentQuestionNumber) {
    if (currentQuestionNumber < ballot.electionMetadata.numberOfQuestions && currentQuestionNumber >=0) {
        displayQuestion(ballot, currentQuestionNumber);
        //deactivate Cast Button
        document.getElementById("button-cast").style.display = null;
        //activate Next Button
        document
            .getElementById("button-next")
            .style
            .display = "block";
        if (
          currentQuestionNumber ==
          ballot.electionMetadata.numberOfQuestions - 1
        ) {
          document
            .getElementById("button-next")
            .addEventListener("click", function() {
              displayReview(ballot, ++currentQuestionNumber);
            });
        }
        else {
            document
              .getElementById("button-next")
              .addEventListener("click", function() {
                currentQuestionNumber++;
                displayBallot(ballot, currentQuestionNumber);
              });
        }

        //activate Back Button
        document
            .getElementById("button-back")
            .style
            .display = "block";
        if (currentQuestionNumber == 0) {
            document
            .getElementById("button-back")
            .addEventListener("click", function() {
              document
              .getElementById("button-next")
              .style
              .display = null;
              document
              .getElementById("button-back")
              .style
              .display = null;
              displayMetadata(ballot);
            });
        }
        else  {
            document
            .getElementById("button-back")
            .addEventListener("click", function() {
                currentQuestionNumber--;
                displayBallot(ballot, currentQuestionNumber);
            });
        }
    }
}