from django.contrib import admin
from django import forms
from .models import Election, EligibleVoter, AuthenticatedVoter, RegisteredVoter, Credential, CredentialShare, RegistrationTeller, CastVote, TabulationTeller, FinalTally, Question, Choice


class ElectionAdminForm(forms.ModelForm):

    class Meta:
        model = Election
        fields = '__all__'


class ElectionAdmin(admin.ModelAdmin):
    form = ElectionAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'organization', 'description',
                    'public_key', 'private_key', 'encrypted_choices', 'key_commitments', 'registration_frozen', 'voting_frozen', 'results_released', 'help_email', 'max_tellers', 'threshold_tellers']
    readonly_fields = ['slug', 'created', 'last_updated', 'public_key', 'private_key', 'encrypted_choices', 'key_commitments', 'registration_frozen', 'voting_frozen', 'results_released']


admin.site.register(Election, ElectionAdmin)


class EligibleVoterAdminForm(forms.ModelForm):

    class Meta:
        model = EligibleVoter
        fields = '__all__'


class EligibleVoterAdmin(admin.ModelAdmin):
    form = EligibleVoterAdminForm
    list_display = ['slug', 'created', 'last_updated']


admin.site.register(EligibleVoter, EligibleVoterAdmin)


class AuthenticatedVoterAdminForm(forms.ModelForm):

    class Meta:
        model = AuthenticatedVoter
        fields = '__all__'


class AuthenticatedVoterAdmin(admin.ModelAdmin):
    form = AuthenticatedVoterAdminForm
    list_display = [ 'slug', 'created', 'last_updated',
                    'authentication_key_public', 'designation_key_public', 'public_index']
    readonly_fields = ['slug', 'created', 'last_updated',
                       'authentication_key_public', 'designation_key_public', 'public_index']


admin.site.register(AuthenticatedVoter, AuthenticatedVoterAdmin)


class RegisteredVoterAdminForm(forms.ModelForm):

    class Meta:
        model = RegisteredVoter
        fields = '__all__'


class RegisteredVoterAdmin(admin.ModelAdmin):
    form = RegisteredVoterAdminForm
    list_display = ['slug', 'created', 'last_updated']


admin.site.register(RegisteredVoter, RegisteredVoterAdmin)


class CredentialAdminForm(forms.ModelForm):

    class Meta:
        model = Credential
        fields = '__all__'


class CredentialAdmin(admin.ModelAdmin):
    form = CredentialAdminForm
    list_display = ['slug', 'created',
                    'last_updated', 'encrypted_credential']


admin.site.register(Credential, CredentialAdmin)


class CredentialShareAdminForm(forms.ModelForm):

    class Meta:
        model = CredentialShare
        fields = '__all__'


class CredentialShareAdmin(admin.ModelAdmin):
    form = CredentialShareAdminForm
    list_display = ['slug', 'created',
                    'last_updated', 'encrypted_credential_share']


admin.site.register(CredentialShare, CredentialShareAdmin)


class RegistrationTellerAdminForm(forms.ModelForm):

    class Meta:
        model = RegistrationTeller
        fields = '__all__'


class RegistrationTellerAdmin(admin.ModelAdmin):
    form = RegistrationTellerAdminForm
    list_display = ['slug', 'created', 'last_updated', 'public_key']
    readonly_fields = ['slug', 'created', 'last_updated', 'public_key']


admin.site.register(RegistrationTeller, RegistrationTellerAdmin)


class CastVoteAdminForm(forms.ModelForm):

    class Meta:
        model = CastVote
        fields = '__all__'


class CastVoteAdmin(admin.ModelAdmin):
    form = CastVoteAdminForm
    list_display = ['slug', 'created', 'last_updated', 'encrypted_credential',
                    'encrypted_choices', 'encrypted_index', 'proof_credential', 'proof_choices', 'proof_index']
    readonly_fields = ['slug', 'created', 'last_updated', 'encrypted_credential',
                       'encrypted_choices', 'encrypted_index', 'proof_credential', 'proof_choices', 'proof_index']


admin.site.register(CastVote, CastVoteAdmin)


class TabulationTellerAdminForm(forms.ModelForm):

    class Meta:
        model = TabulationTeller
        fields = '__all__'


class TabulationTellerAdmin(admin.ModelAdmin):
    form = TabulationTellerAdminForm
    list_display = ['slug', 'created',
                    'last_updated', 'public_key_share']
    readonly_fields = ['slug', 'created',
                       'last_updated', 'public_key_share']


admin.site.register(TabulationTeller, TabulationTellerAdmin)


class FinalTallyAdminForm(forms.ModelForm):

    class Meta:
        model = FinalTally
        fields = '__all__'


class FinalTallyAdmin(admin.ModelAdmin):
    form = FinalTallyAdminForm
    list_display = ['slug', 'created',
                    'last_updated', 'choice_tally', 'proof_tally']
    readonly_fields = ['slug', 'created',
                       'last_updated', 'choice_tally', 'proof_tally']


admin.site.register(FinalTally, FinalTallyAdmin)


class QuestionAdminForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = '__all__'


class QuestionAdmin(admin.ModelAdmin):
    form = QuestionAdminForm
    list_display = ['slug', 'created',
                    'last_updated', 'text', 'description', 'max_choices']
    readonly_fields = ['slug', 'created',
                       'last_updated']


admin.site.register(Question, QuestionAdmin)


class ChoiceAdminForm(forms.ModelForm):

    class Meta:
        model = Choice
        fields = '__all__'


class ChoiceAdmin(admin.ModelAdmin):
    form = ChoiceAdminForm
    list_display = ['slug', 'created', 'last_updated', 'text', 'votes']
    readonly_fields = ['slug', 'created',
                       'last_updated']


admin.site.register(Choice, ChoiceAdmin)
